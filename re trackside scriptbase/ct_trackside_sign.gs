//=============================================================================
// File: ct_trackside_sign.gs
// Desc: Содержит базовую реализацию смещаемых путевых объектов
// Auth: Алексей 'Эрендир' Зверев 2020 © Trainz Dev Team
//       Licence: GNU GPL
//=============================================================================

include "trackside.gs"
include "ecl_bitlogic.gs"
include "string.gs"

//=============================================================================
// Name: CT_TracksideSign
// Desc: Базовый класс для смещаемых путевых объектов 
//=============================================================================
class CT_TracksideSign isclass Trackside
{

  // Name: GetPrevDescriptionHTML
  // Desc: Получает HTML структуру для настроек, расположеную между заголовком и
  //       параметрами смещения. 
  // Parm: strTable — Глобальная таблица строк библиотеки.
  // Parm: strTableLocal — Таблица строк текущего объекта.
  // Retn: HTML представления для настроек. 
  // Note: При необходимости переопределяется в дочернем классе. 
  //=============================================================================
  string GetPrevDescriptionHTML(StringTable strTable, StringTable strTableLocal);
  
  // Name: UpdateObjectOffset
  // Desc: Обновляет положение и ориентацию объекта с заданными параметрами.
  // Note: Вызывается автоматически. Может быть переопределена в дочернем классе
  //       При переопределении требуется вызов inherited(). 
  //=============================================================================
  mandatory void UpdateObjectOffset(void);

	//
	// РЕАЛИЗАЦИЯ
	//

  //Константы параметров
  define int CGG_CANREVERSE       = 0;   //Разрешён разворот на 180 градусов
  define int CFG_AUTOREVERSE      = 1;   //Допускается автоматический разворот на 180 градусов при изменении направления смещения от оси пути
  define int CFG_CANROTATEX       = 2;   //Разрешает поворт по оси X (наклон вдоль оси пути)
  define int CFG_CANROTATEY       = 3;   //Разрешает поворт по оси Y (наклон в сторону от пути)
  define int CFG_CANROTATEZ       = 4;   //Разрешает поворт по оси Z (поворт по вертикальной оси)  
  define int CFG_CANALONGOFFSET   = 5;   //Разрешает смещение вдоль пути
  define int CFG_CANHEIGHTOFFSET  = 6;   //Разрешает смещение по высоте

  int configstate;                        //Хранит флаги конфигурации

  float originoffsetaxis;                 //Изначальное смещение по оси X при установке объекта
  float defaultoffsetaxis;                //Смещение от сои пути по умолчанию (будет отображаться как кнопка)
  float defaultrotatex;                   //Поворот по оси Х по умолчанию
  float defaultrotatey;                   //Поворот по оси Y по умолчанию
  float defaultrotatez;                   //Поворот по оси Z по умолчанию

  float offsetaxis;                       //Смещение от оси пути
  float offestalong;                      //Смещение вдоль оси пути
  float offsetheight;                     //Смещения по высоте
  float rotatex;                          //Поворот по оси Х
  float rotatey;                          //Поворот по оси Y
  float rotatez;                          //Поворот по оси Z (Вокруг оси)
  bool reverse;                           //Указатель, что объект развернут

  
  float[] offsetaxislimit;                //Лимит для перемещения относительно оси пути
  float[] offsetalonglimit;               //Лимит перемещения вдоль оси пути
  float[] offsetheightlinit;              //Лимит смещения по высоте
  float[] rotatexlimit;                   //Лимит поворта по оси X
  float[] rotateylimit;                   //Лимит поворта по оси Y
  float[] rotatezlimit;                   //Лимит поворта по оси Z
     

  mandatory void UpdateObjectOffset(void)
  {
    me.SetMeshTranslation("default", offsetaxis, offestalong, offsetheight);
    float rotanglex = defaultrotatex;
    float rotangley = defaultrotatey;
    float rotanglez = defaultrotatez;
    if(configstate & ((1 << CGG_CANREVERSE) | (1 << CFG_AUTOREVERSE))) {
      if(reverse) rotanglez = rotanglez + Math.PI;
      int rev = rotanglez / (2 * Math.PI);
      rotanglez = rotanglez - rev * 2 * Math.PI;
    }
    if(ECLLogic.ReadBit(configstate, CFG_CANROTATEX)) rotanglex = rotanglex + -rotatex;
    if(ECLLogic.ReadBit(configstate, CFG_CANROTATEY)) rotangley = rotangley + rotatey;
    if(ECLLogic.ReadBit(configstate, CFG_CANROTATEZ)) rotanglez = rotanglez + rotatez;
    me.SetMeshOrientation("default", rotanglex, rotangley, rotanglez);
  }
  
  string GetPrevDescriptionHTML(StringTable strTable, StringTable strTableLocal)
  {
    return "";
  }

  //Выполняет парсинг значения конфигурации с лимитом
  final float[] ParseLimitValue(string value, float min, float max, float step) {
    string[] values = Str.Tokens(value, ",");
    float[] returns = new float[3];
    bool valid = values.size() == 3 or values.size() == 2;
    if(valid) {
      int i;
      for(i = 0; i < values.size(); ++i) {
        returns[i] = Str.UnpackFloat(values[i]);
        Str.TrimLeft(values[i], " ");
        if(values[i] != "") {
          valid = false;
          break;
        }
      }
    }
    if(!valid) {
      returns[0] = min;
      returns[1] = max;
    }
    if(!valid or values.size() == 2) returns[2] = step;
    return returns;
  }
  
  final string LimitToPropertyType(float[] limit) 
  {
    return "float," + limit[0] + "," + limit[1] + "," + limit[2];
  }

  //Перевод градусов в радианы  
  final float GradToRad(float grad)
  {
    return grad * Math.PI / 180;
  }

  //Перевод радиан в градусы
  final float RadToGard(float rad)
  {
    return rad * 180 / Math.PI;
  }

	string GetPropertyType(string id)
	{
    if(id == "setoffsetaxisleft" or id == "setoffsetaxisright" or id == "resetoffsetalong" or id == "setoffsetheightlow"  or id == "resetoffsetheight" or
    id == "resetrotatex" or id == "resetrotatey" or id == "resetrotatez" or id == "reverse") return "link";
		else if(id == "setoffsetaxis") return LimitToPropertyType(offsetaxislimit);
		else if(id == "setoffsetalong") return LimitToPropertyType(offsetalonglimit);
		else if(id == "setoffsetheight") return LimitToPropertyType(offsetheightlinit);
		else if(id == "setoffsetaxis") return LimitToPropertyType(rotatexlimit);
		else if(id == "setrotatex") return LimitToPropertyType(rotatexlimit);
		else if(id == "setrotatey") return LimitToPropertyType(rotateylimit);
		else if(id == "setrotatez") return LimitToPropertyType(rotatezlimit);
		return inherited(id);
	}

  string GetPropertyName(string id)
  {
    StringTable strtablethis = GetAsset().GetStringTable(), strtable;
    Asset libasset = GetAsset().FindAsset("TracksideScriptLibrary");
    if(libasset) strtable = libasset.GetStringTable();
    else strtable = strtablethis;
    if(id == "setoffsetaxis") return strtable.GetString2("interface-offsetaxis", String.FloatToPercentRoundString(offsetaxislimit[0]), String.FloatToPercentRoundString(offsetaxislimit[1]));
    else if(id == "setoffsetalong") return strtable.GetString2("interface-offsetalongaxis", String.FloatToPercentRoundString(offsetalonglimit[0]), String.FloatToPercentRoundString(offsetalonglimit[1]));
    else if(id == "setoffsetheight") return strtable.GetString2("interface-offsetheight", String.FloatToPercentRoundString(offsetheightlinit[0]), String.FloatToPercentRoundString(offsetheightlinit[1]));
    else if(id == "setrotatex") return strtable.GetString2("interface-rotate", String.FloatToPercentRoundString(rotatexlimit[0]), String.FloatToPercentRoundString(rotatexlimit[1]));
    else if(id == "setrotatey") return strtable.GetString2("interface-rotate", String.FloatToPercentRoundString(rotateylimit[0]), String.FloatToPercentRoundString(rotateylimit[1]));
    else if(id == "setrotatez") return strtable.GetString2("interface-rotatez", String.FloatToPercentRoundString(rotatezlimit[0]), String.FloatToPercentRoundString(rotatezlimit[1]));
    return inherited(id);
  }

  public string GetPropertyValue(string id)
  {
    if(id == "setoffsetaxis")return String.FloatToPercentRoundString(offsetaxis);
    else if(id == "setoffsetalong")return String.FloatToPercentRoundString(offestalong);
    else if(id == "setoffsetheight")return String.FloatToPercentRoundString(offsetheight);
    else if(id == "setrotatex")return String.FloatToPercentRoundString(me.RadToGard(rotatex));
    else if(id == "setrotatey")return String.FloatToPercentRoundString(me.RadToGard(rotatey));
    else if(id == "setrotatez")return String.FloatToPercentRoundString(me.RadToGard(rotatez));
    return inherited(id);
  }

  string GetDescriptionHTML(void)
  {
    StringTable strtablethis = GetAsset().GetStringTable(), strtable;
    Asset libasset = GetAsset().FindAsset("TracksideScriptLibrary");
    if(libasset) strtable = libasset.GetStringTable();
    else strtable = strtablethis;

    //Создаём буфер для построение интерфейса    
    HTMLBuffer buffer = HTMLBufferStatic.Construct();
    
    //Тут будет заголовок
    string title = strtablethis.GetString("title");
    if(title == "") title = strtable.GetString("title-sign"); 
    buffer.Print("<font size=10 color=#31859c><b>");
    buffer.Print(title);
    buffer.Print("</b></font><br><br>");

    string prevhtml = GetPrevDescriptionHTML(strtable, strtablethis);
    if(prevhtml != "") {
    buffer.Print(prevhtml);
    buffer.Print("<br>");
    }
                                             
    buffer.Print(HTMLWindow.StartTable("border=0"));
      buffer.Print(HTMLWindow.StartRow());
        buffer.Print(HTMLWindow.MakeCell(HTMLWindow.MakeBold(strtable.GetString("settings-offset")), "colspan=5"));
      buffer.Print(HTMLWindow.EndRow());
      buffer.Print(HTMLWindow.StartRow());
        buffer.Print(HTMLWindow.MakeSpacerCell(10));
        buffer.Print(HTMLWindow.MakeCell(strtable.GetString("offsetaxis") + ":"));
        buffer.Print(HTMLWindow.MakeCell(HTMLWindow.MakeLink("live://property/setoffsetaxis", String.FloatToPercentRoundString(offsetaxis), strtable.GetString("tooltip-offsetaxis")), "width=60"));
        buffer.Print(HTMLWindow.MakeCell(HTMLWindow.MakeFontColor(HTMLWindow.MakeLink("live://property/setoffsetaxisleft", String.FloatToPercentRoundString(-defaultoffsetaxis), strtable.GetString("tooltip-offsetaxis-defleft")), "007acc")));
        buffer.Print(HTMLWindow.MakeCell(HTMLWindow.MakeFontColor(HTMLWindow.MakeLink("live://property/setoffsetaxisright", String.FloatToPercentRoundString(defaultoffsetaxis), strtable.GetString("tooltip-offsetaxis-defright")), "007acc")));
      buffer.Print(HTMLWindow.EndRow());
      if(ECLLogic.ReadBit(configstate, CFG_CANALONGOFFSET)) {
        buffer.Print(HTMLWindow.StartRow());
          buffer.Print(HTMLWindow.MakeSpacerCell(10));
          buffer.Print(HTMLWindow.MakeCell(strtable.GetString("offsetalongaxis") + ":"));
          buffer.Print(HTMLWindow.MakeCell(HTMLWindow.MakeLink("live://property/setoffsetalong", String.FloatToPercentRoundString(offestalong), strtable.GetString("tooltip-offsetalongaxis")), "width=60"));
          buffer.Print(HTMLWindow.MakeCell(HTMLWindow.MakeFontColor(HTMLWindow.MakeLink("live://property/resetoffsetalong", "0.00", strtable.GetString("tooltip-offsetalongaxis-reset")), "007acc"), "colspan=2"));
        buffer.Print(HTMLWindow.EndRow());
      }
      if(ECLLogic.ReadBit(configstate, CFG_CANHEIGHTOFFSET)) {
        buffer.Print(HTMLWindow.StartRow());
          buffer.Print(HTMLWindow.MakeSpacerCell(10));
          buffer.Print(HTMLWindow.MakeCell(strtable.GetString("offsetheight") + ":"));
          buffer.Print(HTMLWindow.MakeCell(HTMLWindow.MakeLink("live://property/setoffsetheight", String.FloatToPercentRoundString(offsetheight), strtable.GetString("tooltip-offsetheight")), "width=60"));
          buffer.Print(HTMLWindow.MakeCell(HTMLWindow.MakeFontColor(HTMLWindow.MakeLink("live://property/setoffsetheightlow", "-0.20", strtable.GetString("tooltip-offsetheight-low")), "007acc")));
          buffer.Print(HTMLWindow.MakeCell(HTMLWindow.MakeFontColor(HTMLWindow.MakeLink("live://property/resetoffsetheight", "0.00", strtable.GetString("tooltip-offsetheight-reset")), "007acc")));
        buffer.Print(HTMLWindow.EndRow());
      }
      if(ECLLogic.ReadBit(configstate, CFG_CANROTATEX)) {
        buffer.Print(HTMLWindow.StartRow());
          buffer.Print(HTMLWindow.MakeSpacerCell(10));
          buffer.Print(HTMLWindow.MakeCell(strtable.GetString("rotatex") + ":"));
          buffer.Print(HTMLWindow.MakeCell(HTMLWindow.MakeLink("live://property/setrotatex", String.FloatToPercentRoundString(RadToGard(rotatex)), strtable.GetString("tooltip-setrotatex")), "width=60"));
          buffer.Print(HTMLWindow.MakeCell(HTMLWindow.MakeFontColor(HTMLWindow.MakeLink("live://property/resetrotatex", strtable.GetString("rotate-default"), strtable.GetString("tooltip-resetrotatex")), "007acc"), "colspan=2"));
        buffer.Print(HTMLWindow.EndRow());
      }
      if(ECLLogic.ReadBit(configstate, CFG_CANROTATEY)) {
        buffer.Print(HTMLWindow.StartRow());
          buffer.Print(HTMLWindow.MakeSpacerCell(10));
          buffer.Print(HTMLWindow.MakeCell(strtable.GetString("rotatey") + ":"));
          buffer.Print(HTMLWindow.MakeCell(HTMLWindow.MakeLink("live://property/setrotatey", String.FloatToPercentRoundString(RadToGard(rotatey)), strtable.GetString("tooltip-setrotatey")), "width=60"));
          buffer.Print(HTMLWindow.MakeCell(HTMLWindow.MakeFontColor(HTMLWindow.MakeLink("live://property/resetrotatey", strtable.GetString("rotate-default"), strtable.GetString("tooltip-resetrotatey")), "007acc"), "colspan=2"));
        buffer.Print(HTMLWindow.EndRow());
      }
      if(ECLLogic.ReadBit(configstate, CFG_CANROTATEZ)) {
        buffer.Print(HTMLWindow.StartRow());
          buffer.Print(HTMLWindow.MakeSpacerCell(10));
          buffer.Print(HTMLWindow.MakeCell(strtable.GetString("rotatez") + ":"));
          buffer.Print(HTMLWindow.MakeCell(HTMLWindow.MakeLink("live://property/setrotatez", String.FloatToPercentRoundString(RadToGard(rotatez)), strtable.GetString("tooltip-setrotatez")), "width=60"));
          buffer.Print(HTMLWindow.MakeCell(HTMLWindow.MakeFontColor(HTMLWindow.MakeLink("live://property/resetrotatez", strtable.GetString("rotate-default"), strtable.GetString("tooltip-resetrotatez")), "007acc"), "colspan=2"));
        buffer.Print(HTMLWindow.EndRow());
      }
      if(ECLLogic.ReadBit(configstate, CGG_CANREVERSE)) {
        buffer.Print(HTMLWindow.StartRow());
          buffer.Print(HTMLWindow.MakeSpacerCell(10));
          buffer.Print(HTMLWindow.MakeCell(strtable.GetString("reverse") + ":"));
          buffer.Print(HTMLWindow.MakeCell(HTMLWindow.CheckBox("live://property/reverse", reverse), "colspan=3"));
        buffer.Print(HTMLWindow.EndRow());
      }
    buffer.Print(HTMLWindow.EndTable());    
    return buffer.AsString();
  }

  void SetPropertyValue(string id, float value)
  {
    if(id == "setoffsetaxis"){
      if(value >= offsetaxislimit[0] and value <= offsetaxislimit[1]) { 
        if(ECLLogic.ReadBit(configstate, CFG_AUTOREVERSE)) {
          if(value < 0 and offsetaxis >= 0) reverse = true;
          else if (value >= 0 and offsetaxis < 0) reverse = false; 
        }
        offsetaxis = value;
        UpdateObjectOffset();
      }
    }else if(id == "setoffsetalong"){
      if(value >= offsetalonglimit[0] and value <= offsetalonglimit[1]) { 
        offestalong = value;
        UpdateObjectOffset();
      }
    }else if(id == "setoffsetheight"){
      if(value >= offsetheightlinit[0] and value <= offsetheightlinit[1]) { 
        offsetheight = value;
        UpdateObjectOffset();
      }
    }else if(id == "setrotatex"){
      if(value >= rotatexlimit[0] and value <= rotatexlimit[1]) { 
        rotatex = me.GradToRad(value);
        UpdateObjectOffset();
      }
    }else if(id == "setrotatey"){
      if(value >= rotateylimit[0] and value <= rotateylimit[1]) { 
        rotatey = me.GradToRad(value);
        UpdateObjectOffset();
      }
    }else if(id == "setrotatez"){
      if(value >= rotatezlimit[0] and value <= rotatezlimit[1]) { 
        rotatez = me.GradToRad(value);
        UpdateObjectOffset();
      }
    }else inherited(id, value);
  }

  void LinkPropertyValue(string id){
    if(id == "setoffsetaxisleft"){
      if(ECLLogic.ReadBit(configstate, CFG_AUTOREVERSE) and offsetaxis >= 0) reverse = true;
      offsetaxis = -defaultoffsetaxis;
      UpdateObjectOffset();
    }else if(id == "setoffsetaxisright"){
      if(ECLLogic.ReadBit(configstate, CFG_AUTOREVERSE) and offsetaxis < 0) reverse = false;
      offsetaxis = defaultoffsetaxis;
      UpdateObjectOffset();
    }else if(id == "resetoffsetalong"){
      offestalong = 0.0;
      UpdateObjectOffset();
    }else if(id == "setoffsetheightlow"){
      offsetheight = -0.2;
      UpdateObjectOffset();
    }else if(id == "resetoffsetheight"){
      offsetheight = 0.0;
      UpdateObjectOffset();
    }else if(id == "reverse"){
      reverse = ECLLogic.ReadBit(configstate, CGG_CANREVERSE) and !reverse;
      UpdateObjectOffset();
    }else if(id == "resetrotatex"){
      rotatex = 0.0;
      UpdateObjectOffset();
    }else if(id == "resetrotatey"){
      rotatey = 0.0;
      UpdateObjectOffset();
    }else if(id == "resetrotatez"){
      rotatez = 0.0;
      UpdateObjectOffset();
    }else inherited(id);
  }
  
  public Soup GetProperties(void)
  {
    Soup soup = inherited();
    soup.SetNamedTag("OffsetAxis", offsetaxis);
    soup.SetNamedTag("OffsetAlongAxis", offestalong);
    soup.SetNamedTag("OffsetHeight", offsetheight);
    soup.SetNamedTag("Reverse", reverse);
    soup.SetNamedTag("RotateX", rotatex);
    soup.SetNamedTag("RotateY", rotatey);
    soup.SetNamedTag("RotateZ", rotatez);
    return soup;
  }
  
	public void SetProperties(Soup soup)
  {
    inherited(soup);
    offsetaxis = soup.GetNamedTagAsFloat("OffsetAxis", originoffsetaxis);
    offestalong = soup.GetNamedTagAsFloat("OffsetAlongAxis");
    offsetheight = soup.GetNamedTagAsFloat("OffsetHeight");
    reverse = soup.GetNamedTagAsBool("Reverse");
    rotatex = soup.GetNamedTagAsFloat("RotateX");
    rotatey = soup.GetNamedTagAsFloat("RotateY");
    rotatez = soup.GetNamedTagAsFloat("RotateZ");
    reverse = (configstate & ((1 << CGG_CANREVERSE) | (1 << CFG_AUTOREVERSE))) and reverse;
    UpdateObjectOffset();
  }

  public mandatory void Init(Asset myAsset)
  {
    inherited(myAsset);

    //Загрузка конфигурации
    Soup config = myAsset.GetConfigSoup().GetNamedSoup("extensions");
    bool oldconfig = config.GetIndexForNamedTag("trackside-settings") == -1; 
    if(!oldconfig) config = config.GetNamedSoup("extensions");  //Если присутствует блок настроек, то получаем его - иначе оставляем базовый блок для совместимости со старой версией
    
    originoffsetaxis = config.GetNamedTagAsFloat("startoffsetaxis", 3.1);
    defaultoffsetaxis = Math.Fabs(config.GetNamedTagAsFloat("defaultoffsetaxis", 3.1));
    configstate = ECLLogic.SetBit(configstate, CGG_CANREVERSE, config.GetNamedTagAsBool("canreverse", false));
    configstate = ECLLogic.SetBit(configstate, CFG_AUTOREVERSE, config.GetNamedTagAsBool("autoreverse", false));
    configstate = configstate | ((config.GetNamedTagAsInt("canrotate", 0) << 2) & ((1 << CFG_CANROTATEX) | (1 << CFG_CANROTATEY) | (1 << CFG_CANROTATEZ)));
    if(!oldconfig) {
      configstate = ECLLogic.SetBit(configstate, CFG_CANALONGOFFSET, config.GetNamedTagAsBool("autoreverse", true));
      configstate = ECLLogic.SetBit(configstate, CFG_CANHEIGHTOFFSET, config.GetNamedTagAsBool("canheightoffest", true));
      offsetaxislimit = ParseLimitValue(config.GetNamedTag("offsetaxislimit"), -10, 10, 0.1); 
      offsetalonglimit = ParseLimitValue(config.GetNamedTag("offsetalongaxislimit"), -1, 1, 0.1); 
      offsetheightlinit = ParseLimitValue(config.GetNamedTag("offsetheightlimit"), -10, 10, 0.1); 
      rotatexlimit = ParseLimitValue(config.GetNamedTag("rotatelimit-x"), -90, 90, 0.5); 
      rotateylimit = ParseLimitValue(config.GetNamedTag("rotatelimit-y"), -90, 90, 0.5); 
      rotatezlimit = ParseLimitValue(config.GetNamedTag("rotatelimit-z"), -180, 180, 1.0); 
    } else {
      configstate = ECLLogic.SetBit(configstate, CFG_CANALONGOFFSET, true);
      configstate = ECLLogic.SetBit(configstate, CFG_CANHEIGHTOFFSET, true);
      offsetaxislimit = ParseLimitValue("", -10, 10, 0.1); 
      offsetalonglimit = ParseLimitValue("", -1, 1, 0.1); 
      offsetheightlinit = ParseLimitValue("", -10, 10, 0.1); 
      rotatexlimit = ParseLimitValue("", -90, 90, 0.5); 
      rotateylimit = ParseLimitValue("", -90, 90, 0.5); 
      rotatezlimit = ParseLimitValue("", -180, 180, 1.0); 
    }
   
    //Получение изначальной ориентации объекта
    string[] defaultrotation = Str.Tokens(myAsset.GetConfigSoup().GetNamedSoup("mesh-table").GetNamedSoup("default").GetNamedTag("orientation"), ",");
    if(defaultrotation.size() == 3){
      defaultrotatex = Str.ToFloat(defaultrotation[0]);
      defaultrotatey = Str.ToFloat(defaultrotation[1]);
      defaultrotatez = Str.ToFloat(defaultrotation[2]);
    }
  }

};
