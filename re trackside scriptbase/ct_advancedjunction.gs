//=============================================================================
// File: ct_advancedjunction.gs
// Desc: Содержит реализацию смещаемых путевых объектов стрелочного перевода
//       с дополнительным функционалом.
// Auth: Алексей 'Эрендир' Зверев 2020 © Trainz Dev Team
//       Licence: GNU GPL
//=============================================================================

include "ct_tracksideprovider.gs"
include "ecl_bitlogic.gs"
include "junction.gs"

//=============================================================================
// Name: ALSN_State
// Desc: Класс с реализацией улучшеного стрелочного перевода с дополнительной
//       информацией используемой маршрутизацией 
//=============================================================================
class CT_AdvancedJunction isclass Junction
{
  
  //=============================================================================
  // Name: BRANCH_*
  // Desc: Набор констант, определяющих тип стрелочного перевода
  //=============================================================================
  public define int BRANCH_NONE       = 0;            //Отклонение не определено
  public define int BRANCH_SYMMETRY   = 1;            //Симметричная стрелка
  public define int BRANCH_LEFT       = 2;            //Отклонение влево
  public define int BRANCH_RIGHT      = 4;            //Отклонение вправо

  //=============================================================================
  // Name: GetJunctionType
  // Desc: Определяет тип стрелочного перевода
  // Retn: Значение одной из констант BRANCH_*.
  //=============================================================================
  public final int GetJunctionType(void);

  //=============================================================================
  // Name: JunctionIsManual
  // Desc: Возвращает занчение true - если срелка является ручной; в противном
  //       случае — значение false.
  //=============================================================================
  public final bool JunctionIsManual(void);

  //=============================================================================
  // Name: JunctionIsAutoReturn
  // Desc: Возвращает занчение true - если срелка имеет само возврат в исходное
  //       положение; в противном случае — значение false.
  //=============================================================================
  public final bool JunctionIsAutoReturn(void);
  
  //=============================================================================
  // Name: GetJunctionNumber
  // Desc: Возвращает номер стрелки на станции
  //=============================================================================
  public final int GetJunctionNumber(void);
  
  //=============================================================================
  // Name: GetDirectionLeftMultiplier
  // Desc: Возвращает множитель по левому направлению
  //=============================================================================
  public final int GetDirectionLeftMultiplier(void);

  //=============================================================================
  // Name: GetDirectionRightMultiplier
  // Desc: Возвращает множитель по правому направлению
  //=============================================================================
  public final int GetDirectionRightMultiplier(void);

  //=============================================================================
  // Name: OnJunctionTypeChanged
  // Desc: Возникает при изменении типа стрелочного перевода.
  // Note: Вызывается автоматически. Для реализации необходимо переопределить
  //       в наследуемом классе.
  //=============================================================================
  void OnJunctionTypeChanged(void){}

  //=============================================================================
  // Name: OnJunctionTypeChanged
  // Desc: Возникает при изменении направления стрелки.
  // Note: Вызывается автоматически. Для реализации необходимо переопределить
  //       в наследуемом классе.
  //=============================================================================
  void OnJunctionDirectionChanged(void){}

  //=============================================================================
  // Name: OnJunctionTypeChanged
  // Desc: Возникает при изменении номера стрелки на станции.
  // Note: Вызывается автоматически. Для реализации необходимо переопределить
  //       в наследуемом классе.
  //=============================================================================
  void OnJunctionNumberChanged(void){}

	//
	// РЕАЛИЗАЦИЯ
	//
  
  define int CFG_CANOFFSET            = 1 << 0;       //Флаг разрешения смещения рычага вдоль пути
  define int CFG_CANNUMBERVISIBLE     = 1 << 1;       //Флаг разрешения отображения номера
  define int CFG_NUMBERALIGN          = 1 << 2;       //Флаг необходимости выравниания номера (при отсутствиии вырванивание по центру)
  define int CFG_ALIGNFAR             = 1 << 3;       //Флаг выравнивания по дальней стороне
  define int CFG_VERTICALNUMBER       = 1 << 4;       //Указатель, что номер отображается вертикально
  define int CFG_INVERTANIMATION      = 1 << 5;       //Инвертирование анимации стрелки 
  define int CFG_AUTOJUNCTION         = 1 << 6;       //Централизованная стрела
  define int CFG_MANUALJUNCTION       = 1 << 7;       //Ручная стрелка 
    

  CT_TracksideProvider _library;                      //Библиотека
  MeshObject _toggle;                                 //Непосредственно объект сетки с переключателем
  Asset _letter;                                      //Актив, содержащий литеру для номера
  Asset _numbers;                                     //Актив с текстурами цфир для номера
  Asset _soundlibrary;                                //Библиотека звуков
  int _configflags                    = 0;            //Набор флагов конфигурации для стрелки            
  int _type                           = BRANCH_NONE;  //Тип стрелки
  int _leftmultiplier                 = 0;            //Множитель по левому направлению
  int _rightmultiplier                = 0;            //Множитель по правому направлению
  int _number                         = 0;            //Номер стрелки
  int _numberlength                   = 3;            //Длина отображаемого номера
  int _backwardoffset                 = 100;          //Максимальное смещение назад (см) 
  int _forwardoffset                  = 100;          //Максимальное смещение вперёд (см)
  int _offset                         = 0;            //Текущее смещение (см)
  float _numberlitteraseparator       = 0.0;          //Промежуток между символами номера стрелки
  float _numberliteralsize            = 0.0;          //Ширина или высота символа/таблички с символом номера 
  bool _manual                        = false;        //Указатель, что стрелка является ручной
  bool _autoreturn                    = false;        //Стрелка с самовозвратом
  bool _numbervisible                 = true;         //Указатель необходимости отображения номера
  string _soundfileid;                                //Идентификатор звука, который выбран
  string _soundpoint;                                 //Хранит точку, на которой должен проигрываться звук
  string _toggleanimationmesg;                        //Наименованеи сетки, которая анимируется при переключении
  
  
  
                      

  public final int GetJunctionType(void) 
  {
    return _type;
  }

  public final bool JunctionIsManual(void)
  {
    return _manual;
  }

  public final bool JunctionIsAutoReturn(void)
  {
    return !_manual and _autoreturn;
  }

  public final int GetJunctionNumber(void)
  {
    return _number;
  }
  
  public final int GetDirectionLeftMultiplier(void)
  {
    return _leftmultiplier;
  }
  
  public final int GetDirectionRightMultiplier(void)
  {
    return _rightmultiplier;
  }
  
  final void ConfirmOffset()
  {
    if (_configflags & CFG_CANOFFSET) {
      _offset = Math.Min(Math.Max(_offset, -_backwardoffset), _forwardoffset);
      _toggle.SetMeshTranslation("default", 0.0, _offset * 0.01, 0.0);
    }
  }
  
  final void UpdateNumberLetter()
  {
    if (_configflags & CFG_CANNUMBERVISIBLE) {
      string junctionnumber = (string)_number;
      if (!_numbervisible or _number == 0 or junctionnumber.size() > _numberlength) junctionnumber = "";
      int i, nsize = junctionnumber.size();
      for (i = 0; i < _numberlength; ++i) {
        if (i < nsize) {
          MeshObject currletter = _toggle.GetFXAttachment("number-" + (i + 1));
          if (!currletter) currletter = _toggle.SetFXAttachment("number-" + (i + 1), _letter);
          float position = 0.0;
          if (ECLLogic.GetBitMask(_configflags, CFG_NUMBERALIGN | CFG_ALIGNFAR)) position = -((_numberliteralsize + _numberlitteraseparator) * (nsize - (i + 1)));
          else if (_configflags & CFG_NUMBERALIGN) position = (_numberliteralsize + _numberlitteraseparator) * i;
          else position = -((_numberliteralsize * nsize + _numberlitteraseparator * (nsize - 1)) * 0.5) + (_numberliteralsize + _numberlitteraseparator) * i;
          if (_configflags & CFG_VERTICALNUMBER) currletter.SetMeshTranslation("default", 0.0, -position, 0.0);
          else currletter.SetMeshTranslation("default", position, 0.0, 0.0); 
          currletter.SetFXTextureReplacement("letter", _numbers, Str.ToInt(junctionnumber[i]) - 48);        
        } else _toggle.SetFXAttachment("number-" + (i + 1), null); 
      }
    }
  }
  

/*  void UpdateLetter(string effetBaseName, string number, float k)
  {
    float[] positions = GetLetterPosition(number.size());
    int i;
    for(i = 0; i < 4; ++i) {
      if(i < number.size()){
        MeshObject curletter = GetFXAttachment(effetBaseName + (i + 1));
        if(!curletter) curletter = SetFXAttachment(effetBaseName + (i + 1), letter);
        curletter.SetMeshTranslation("default", positions[i] - k, 0.0, 0.0);
        curletter.SetFXTextureReplacement("letter", numbers, Str.ToInt(number[i]) - 48);        
      } else SetFXAttachment(effetBaseName + (i + 1), null);
    }
  }
  */
  
  
  final void PlayJunctionSound(string fileid, string point) 
  {
    if(_soundlibrary) {
      Soup fileconf = _soundlibrary.GetConfigSoup().GetNamedSoup("extensions").GetNamedSoup("advancedjunction-sounds").GetNamedSoup("sounds").GetNamedSoup(fileid);
      string soundfile = fileconf.GetNamedTag("file");
      if(soundfile != "") World.PlaySound(_soundlibrary, soundfile, 1.0, 2.0, 10.0, me, point);
      else World.Play2DSound(_soundlibrary, soundfile);
    }
  }
  
  final void ToggleAnimation(void)
  {
    if (_toggleanimationmesg.size() and _toggle.HasMesh(_toggleanimationmesg)) {
      bool animstate = (!(_configflags & CFG_INVERTANIMATION) and GetDirection() == JunctionBase.DIRECTION_RIGHT) or (_configflags & CFG_INVERTANIMATION and GetDirection() == JunctionBase.DIRECTION_LEFT);   
      _toggle.SetMeshAnimationState(_toggleanimationmesg, animstate);
    }
  }
  
  public final void ToggledJunctionHandler(Message msg)
  {
    ToggleAnimation();
    if(_soundpoint != "") PlayJunctionSound(_soundfileid, _soundpoint);
    OnJunctionDirectionChanged();    
  }
  
  final bool SoundLibraryValid(Soup soundLibraryConfig, StringTable strTable) {
    string titlestring = soundLibraryConfig.GetNamedTag("title-string");
    if(titlestring == "" or strTable.GetString(titlestring) == "") return false;
    Soup soundconf = soundLibraryConfig.GetNamedSoup("sounds");
    int count = soundconf.CountTags(), i;
    if(count == 0) return false;
    for(i = 0; i < count; ++i) {
      Soup fileconf = soundconf.GetNamedSoup(soundconf.GetIndexedTagName(i));
      string filetitlestring = fileconf.GetNamedTag("title-string");
      if(filetitlestring == "" or fileconf.GetNamedTag("file") == "" or strTable.GetString(filetitlestring) == "filetitlestring") return false;
    }
    return true;
  }
   
  final string GetJunctionTypeString(StringTable strTable)
  {
    if(_type & (BRANCH_LEFT | BRANCH_RIGHT)) return strTable.GetString("ajunction-type-asymmetry");
    else if(_type == BRANCH_SYMMETRY) return strTable.GetString("ajunction-type-symmetry");
    return strTable.GetString("ajunction-type-none");
  }

  final string GetJunctionBranchString(StringTable strTable)
  {
    if(_type & BRANCH_LEFT) return strTable.GetString("ajunction-branch-left");
    else if(_type == BRANCH_RIGHT) return strTable.GetString("ajunction-branch-right");
    return "???";
  }
  
  final string GetSoundLibraryTitle() {
    if(_soundlibrary){
        Soup soundconf = _soundlibrary.GetConfigSoup().GetNamedSoup("extensions").GetNamedSoup("advancedjunction-sounds");
        StringTable strtable = _soundlibrary.GetStringTable();
        return strtable.GetString(soundconf.GetNamedTag("title-string")); 
    }
    return "";
  }

  string GetPropertyType(string propertyID)
  {
    if(propertyID == "junctiontype" or propertyID == "junctionbranch" or propertyID == "junctionmanual" or propertyID == "junctionautoreturn" or 
       propertyID == "junctionautoreturn" or propertyID == "resetsoundlibrary" or propertyID[0, 16] == "selectsoundfile." or propertyID[0, 13] == "playsoundile." or propertyID == "numbervisible") return "link";
    else if(propertyID == "junctionmultiplier-left" or propertyID == "junctionmultiplier-right") return "int,0,100";
    else if(propertyID == "junctionnumber") return "int,1,10000";
    else if (propertyID == "offset") return "int," + _backwardoffset + "," + _forwardoffset; 
    else if(propertyID == "selectsoundlibrary") return "list,1";
    return inherited(propertyID);
  }
  
  string[] GetPropertyElementList(string propertyID) {
    if(propertyID == "selectsoundlibrary") {
      CT_SoundLibraryEntry[] items = _library.GetAdvancedJunctionSoundList();
      string[] returns = new string[items.size()];
      int i = 0;
      for(i = 0; i < items.size(); ++i)
        returns[i] = items[i].Title;
      return returns;
    }
    return inherited(propertyID);
  }

  public string GetPropertyValue(string propertyID)
  {
    if(propertyID == "junctionmultiplier-left") return (string)_leftmultiplier;
    else if(propertyID == "junctionmultiplier-right") return (string)_rightmultiplier;
    else if(propertyID == "junctionnumber") return (string)_number;
    else if(propertyID == "offset") return (string)_offset;
    return inherited(propertyID);
  }

  string GetPropertyName(string propertyID)
  {
    StringTable strtable = GetAsset().FindAsset("TracksideScriptLibrary").GetStringTable();
    if(propertyID == "junctionmultiplier-left" and _type & BRANCH_LEFT) return strtable.GetString("interface-ajunction-direction-branch");
    else if(propertyID == "junctionmultiplier-left" and _type & BRANCH_RIGHT) return strtable.GetString("interface-ajunction-direction-direct");
    else if(propertyID == "junctionmultiplier-left") return strtable.GetString("interface-ajunction-direction-left");
    else if(propertyID == "junctionmultiplier-right" and _type & BRANCH_LEFT) return strtable.GetString("interface-ajunction-direction-direct");
    else if(propertyID == "junctionmultiplier-right" and _type & BRANCH_RIGHT) return strtable.GetString("interface-ajunction-direction-branch");
    else if(propertyID == "junctionmultiplier-right") return strtable.GetString("interface-ajunction-direction-right");
    else if(propertyID == "junctionnumber") return strtable.GetString("interface-ajunction-number");
    else if(propertyID == "selectsoundlibrary") return strtable.GetString("interface-ajunction-soundlibrary");
    else if(propertyID == "offset") return strtable.GetString2("interface-ajunction-offset", -_backwardoffset, _forwardoffset);
    return inherited(propertyID);
  }

  string GetDescriptionHTML(void)
  {
    StringTable strtable = GetAsset().FindAsset("TracksideScriptLibrary").GetStringTable();
    HTMLBuffer buffer = HTMLBufferStatic.Construct();
    
    string title = strtable.GetString("title-advancedjunction"); 
    buffer.Print("<font size=10 color=#31859c><b>");
    buffer.Print(title);
    buffer.Print("</b></font><br><br>");

  /*
    buffer.Print(HTMLWindow.StartTable("border=0"));
      buffer.Print(HTMLWindow.StartRow());
        buffer.Print(HTMLWindow.MakeCell("", "width=100%"));
        buffer.Print(HTMLWindow.MakeCell("<nowrap>" + "по умолчанию" + "</nowrap>"));
        buffer.Print(HTMLWindow.MakeCell("<nowrap>" + "скопировать" + "</nowrap>"));
        buffer.Print(HTMLWindow.MakeCell("<nowrap>" + "вставить" + "</nowrap>"));
      buffer.Print(HTMLWindow.EndRow());
    buffer.Print(HTMLWindow.EndTable());
    */



    buffer.Print(HTMLWindow.StartTable("border=0"));
      buffer.Print(HTMLWindow.StartRow());
        buffer.Print(HTMLWindow.MakeCell(HTMLWindow.MakeBold(strtable.GetString("settings-ajunction-type")), "colspan=3"));
      buffer.Print(HTMLWindow.EndRow());
      buffer.Print(HTMLWindow.StartRow());
        buffer.Print(HTMLWindow.MakeSpacerCell(10));
        buffer.Print(HTMLWindow.MakeCell(strtable.GetString("ajunction-type") + ":"));
        buffer.Print(HTMLWindow.MakeCell(HTMLWindow.MakeLink("live://property/junctiontype", GetJunctionTypeString(strtable), strtable.GetString("tooltip-ajunction-type"))));
      buffer.Print(HTMLWindow.EndRow());
      if(_type & (BRANCH_LEFT | BRANCH_RIGHT)) {
        buffer.Print(HTMLWindow.StartRow());
          buffer.Print(HTMLWindow.MakeSpacerCell(10));
          buffer.Print(HTMLWindow.MakeCell(strtable.GetString("ajunction-branch") + ":"));
          buffer.Print(HTMLWindow.MakeCell(HTMLWindow.MakeLink("live://property/junctionbranch", GetJunctionBranchString(strtable), strtable.GetString("tooltip-ajunction-branch"))));
        buffer.Print(HTMLWindow.EndRow());
      }
      if (!(_configflags & (CFG_AUTOJUNCTION | CFG_MANUALJUNCTION))) {
        buffer.Print(HTMLWindow.StartRow());
          buffer.Print(HTMLWindow.MakeSpacerCell(10));
          buffer.Print(HTMLWindow.MakeCell(strtable.GetString("ajunction-manual") + ":"));
          buffer.Print(HTMLWindow.MakeCell(HTMLWindow.CheckBox("live://property/junctionmanual", _manual)));
        buffer.Print(HTMLWindow.EndRow());
      }
      if(!_manual) {
        buffer.Print(HTMLWindow.StartRow());
          buffer.Print(HTMLWindow.MakeSpacerCell(10));
          buffer.Print(HTMLWindow.MakeCell(strtable.GetString("ajunction-autoreturn") + ":"));
          buffer.Print(HTMLWindow.MakeCell(HTMLWindow.CheckBox("live://property/junctionautoreturn", _autoreturn)));
        buffer.Print(HTMLWindow.EndRow());
      }
      buffer.Print(HTMLWindow.StartRow());
        buffer.Print(HTMLWindow.MakeCell(HTMLWindow.MakeBold(strtable.GetString("settings-ajunction-multiplier")), "colspan=3"));
      buffer.Print(HTMLWindow.EndRow());
      if(_type & BRANCH_LEFT) {
        buffer.Print(HTMLWindow.StartRow());
          buffer.Print(HTMLWindow.MakeSpacerCell(10));
          buffer.Print(HTMLWindow.MakeCell(strtable.GetString("ajunction-direction-direct") + ":"));
          buffer.Print(HTMLWindow.MakeCell(HTMLWindow.MakeLink("live://property/junctionmultiplier-right", _rightmultiplier, strtable.GetString("tooltip-ajunction-direction-direct"))));
        buffer.Print(HTMLWindow.EndRow());
        buffer.Print(HTMLWindow.StartRow());
          buffer.Print(HTMLWindow.MakeSpacerCell(10));
          buffer.Print(HTMLWindow.MakeCell(strtable.GetString("ajunction-direction-branch") + ":"));
          buffer.Print(HTMLWindow.MakeCell(HTMLWindow.MakeLink("live://property/junctionmultiplier-left", _leftmultiplier, strtable.GetString("tooltip-ajunction-direction-branch"))));
        buffer.Print(HTMLWindow.EndRow());
      } else if(_type & BRANCH_RIGHT) {
        buffer.Print(HTMLWindow.StartRow());
          buffer.Print(HTMLWindow.MakeSpacerCell(10));
          buffer.Print(HTMLWindow.MakeCell(strtable.GetString("ajunction-direction-direct") + ":"));
          buffer.Print(HTMLWindow.MakeCell(HTMLWindow.MakeLink("live://property/junctionmultiplier-left", _leftmultiplier, strtable.GetString("tooltip-ajunction-direction-direct"))));
        buffer.Print(HTMLWindow.EndRow());
        buffer.Print(HTMLWindow.StartRow());
          buffer.Print(HTMLWindow.MakeSpacerCell(10));
          buffer.Print(HTMLWindow.MakeCell(strtable.GetString("ajunction-direction-branch") + ":"));
          buffer.Print(HTMLWindow.MakeCell(HTMLWindow.MakeLink("live://property/junctionmultiplier-right", _rightmultiplier, strtable.GetString("tooltip-ajunction-direction-branch"))));
        buffer.Print(HTMLWindow.EndRow());
      } else {
        buffer.Print(HTMLWindow.StartRow());
          buffer.Print(HTMLWindow.MakeSpacerCell(10));
          buffer.Print(HTMLWindow.MakeCell(strtable.GetString("ajunction-direction-left") + ":"));
          buffer.Print(HTMLWindow.MakeCell(HTMLWindow.MakeLink("live://property/junctionmultiplier-left", _leftmultiplier, strtable.GetString("tooltip-ajunction-direction-left"))));
        buffer.Print(HTMLWindow.EndRow());
        buffer.Print(HTMLWindow.StartRow());
          buffer.Print(HTMLWindow.MakeSpacerCell(10));
          buffer.Print(HTMLWindow.MakeCell(strtable.GetString("ajunction-direction-right") + ":"));
          buffer.Print(HTMLWindow.MakeCell(HTMLWindow.MakeLink("live://property/junctionmultiplier-right", _rightmultiplier, strtable.GetString("tooltip-ajunction-direction-right"))));
        buffer.Print(HTMLWindow.EndRow());
      }
      buffer.Print(HTMLWindow.StartRow());
        buffer.Print(HTMLWindow.MakeCell(HTMLWindow.MakeBold(strtable.GetString("settings-extension")), "colspan=3"));
      buffer.Print(HTMLWindow.EndRow());
      buffer.Print(HTMLWindow.StartRow());
        buffer.Print(HTMLWindow.MakeSpacerCell(10));
        buffer.Print(HTMLWindow.MakeCell(strtable.GetString("ajunction-number") + ":"));
        buffer.Print(HTMLWindow.MakeCell(HTMLWindow.MakeLink("live://property/junctionnumber", _number, strtable.GetString("tooltip-ajunction-number"))));
      buffer.Print(HTMLWindow.EndRow());
      if (_configflags & CFG_CANNUMBERVISIBLE) {
        buffer.Print(HTMLWindow.StartRow());
          buffer.Print(HTMLWindow.MakeSpacerCell(10));
          buffer.Print(HTMLWindow.MakeCell(strtable.GetString("ajunction-number-visible") + ":"));
          buffer.Print(HTMLWindow.MakeCell(HTMLWindow.CheckBox("live://property/numbervisible", _numbervisible)));
        buffer.Print(HTMLWindow.EndRow());
      }
      if (_configflags & CFG_CANOFFSET) {
        buffer.Print(HTMLWindow.StartRow());
          buffer.Print(HTMLWindow.MakeSpacerCell(10));
          buffer.Print(HTMLWindow.MakeCell(strtable.GetString("offsetalongaxis") + ":"));
          buffer.Print(HTMLWindow.MakeCell(HTMLWindow.MakeLink("live://property/offset", strtable.GetString1("offset-value-cm", _offset), strtable.GetString("tooltip-ajunction-offset"))));
        buffer.Print(HTMLWindow.EndRow());
      }
      if(_soundpoint != ""){
        buffer.Print(HTMLWindow.StartRow());
          buffer.Print(HTMLWindow.MakeCell(HTMLWindow.MakeBold(strtable.GetString("settings-sounds")), "colspan=3"));
        buffer.Print(HTMLWindow.EndRow());
        buffer.Print(HTMLWindow.StartRow());
          buffer.Print(HTMLWindow.MakeSpacerCell(10));
          buffer.Print(HTMLWindow.MakeCell(strtable.GetString("sound-library") + ":"));
          buffer.Print(HTMLWindow.MakeCell(HTMLWindow.MakeFontColor(GetSoundLibraryTitle(), "ddd9c3")));
          buffer.Print(HTMLWindow.MakeCell(HTMLWindow.MakeFontColor(HTMLWindow.MakeLink("live://property/selectsoundlibrary", strtable.GetString("action-select"), strtable.GetString("tooltip-selectsoundlibrary")), "007acc")));
          if(_soundlibrary) buffer.Print(HTMLWindow.MakeCell(HTMLWindow.MakeFontColor(HTMLWindow.MakeLink("live://property/resetsoundlibrary", strtable.GetString("action-delete"), strtable.GetString("tooltip-resetsoundlibrary")), "c00000")));
          else buffer.Print(HTMLWindow.MakeCell(HTMLWindow.MakeFontColor(strtable.GetString("action-delete"), "7f7f7f")));
        buffer.Print(HTMLWindow.EndRow());
      }
    buffer.Print(HTMLWindow.EndTable());
    if(_soundlibrary){
      Soup soudentrylist = _soundlibrary.GetConfigSoup().GetNamedSoup("extensions").GetNamedSoup("advancedjunction-sounds").GetNamedSoup("sounds");
      StringTable soundlibrarystringtable = _soundlibrary.GetStringTable();
      buffer.Print("<br />");      
      buffer.Print(HTMLWindow.StartTable("border=0"));
      int count = soudentrylist.CountTags(), i;
      for(i = 0; i < count; ++i) {
          string soundfileentryid = soudentrylist.GetIndexedTagName(i); 
          Soup fileconf = soudentrylist.GetNamedSoup(soundfileentryid);
          buffer.Print(HTMLWindow.StartRow());
            buffer.Print(HTMLWindow.MakeSpacerCell(10));
            buffer.Print(HTMLWindow.MakeCell(HTMLWindow.RadioButton("live://property/selectsoundfile." + soundfileentryid, soundfileentryid == _soundfileid)));
            buffer.Print(HTMLWindow.MakeCell(soundlibrarystringtable.GetString(fileconf.GetNamedTag("title-string"))));
            buffer.Print(HTMLWindow.MakeCell(HTMLWindow.MakeFontColor(HTMLWindow.MakeLink("live://property/playsoundile." + soundfileentryid, strtable.GetString("sound-play"), strtable.GetString("tooltip-selectsoundlibrary")), "007acc")));
          buffer.Print(HTMLWindow.EndRow());
      }
      buffer.Print(HTMLWindow.EndTable());
    }    
    return buffer.AsString();
  }

  void LinkPropertyValue(string propertyID)
  {
    if(propertyID == "junctiontype"){
      if(_type & (BRANCH_LEFT | BRANCH_RIGHT)) _type = BRANCH_NONE;
      else if(_type == BRANCH_SYMMETRY) _type = BRANCH_LEFT;
      else _type = BRANCH_SYMMETRY;
      me.OnJunctionTypeChanged();
    }else if(propertyID == "junctionbranch"){
      if(_type & BRANCH_LEFT) _type = BRANCH_RIGHT;
      else _type = BRANCH_LEFT;
      me.OnJunctionTypeChanged();
    }else if(propertyID == "junctionmanual"){
      _manual = !_manual;
      me.OnJunctionTypeChanged();
    }else if(propertyID == "junctionautoreturn"){
      _autoreturn = !_autoreturn;
      me.OnJunctionTypeChanged();
    }else if(propertyID == "resetsoundlibrary"){
      _soundlibrary = null;
      _soundfileid = ""; 
    }else if(propertyID[0, 16] == "selectsoundfile.") {
      if(_soundlibrary) _soundfileid = propertyID[16, ];
    } else if(propertyID == "numbervisible") {
     _numbervisible = !_numbervisible;
      UpdateNumberLetter();
    }else if(propertyID[0, 13] == "playsoundile.") PlayJunctionSound(propertyID[13, ], "");
    else inherited(propertyID);
  }

  void SetPropertyValue(string propertyID, int value)
  {
    if(propertyID == "junctionmultiplier-left") {
      if(value >= 0 and value <= 100) _leftmultiplier = value;
    } else if(propertyID == "junctionmultiplier-right") {
      if(value >= 0 and value <= 100) _rightmultiplier = value;
    } else if(propertyID == "junctionnumber") {
      if(value > 0 and _number != value){
        _number = value;
        UpdateNumberLetter();
        OnJunctionNumberChanged();
      }
    } else if(propertyID == "offset") {
      _offset = Math.Min(Math.Max(value, -_backwardoffset), _forwardoffset);
      ConfirmOffset();
    }
    else inherited(propertyID, value);
  }
  
  void SetPropertyValue(string propertyID, string value, int index) 
  {
    if(propertyID == "selectsoundlibrary") {
      CT_SoundLibraryEntry entry = _library.GetAdvancedJunctionSound(index);
      if(entry) _soundlibrary = entry.Asset;
      else _soundlibrary = null;
      _soundfileid = ""; 
    } else inherited(propertyID, value, index);
  }  
  
	public Soup GetProperties(void)
	{
		Soup soup = inherited();
    soup.SetNamedTag("JunctionType", _type);
    soup.SetNamedTag("JunctionLeftMultiplier", _leftmultiplier);
    soup.SetNamedTag("JunctionRightMultiplier", _rightmultiplier);
    soup.SetNamedTag("JunctionNumber", _number);
    soup.SetNamedTag("JunctionIsManual", _manual);
    soup.SetNamedTag("JunctionAutoReturn", _autoreturn);
    soup.SetNamedTag("JunctionOffset", _offset);
    soup.SetNamedTag("JunctionNumberVisible", _numbervisible);
    if(_soundlibrary and _soundfileid != "") {
      soup.SetNamedTag("JunctionSoundLibrary", _soundlibrary.GetKUID());
      soup.SetNamedTag("JunctionSoundFileId", _soundfileid);
    }
		return soup;
	}

  public void SetProperties(Soup soup)
	{
		inherited(soup);
    int type = soup.GetNamedTagAsInt("JunctionType", _type) & (BRANCH_SYMMETRY | BRANCH_LEFT | BRANCH_RIGHT); 
    _leftmultiplier = Math.Max(soup.GetNamedTagAsInt("JunctionLeftMultiplier", _leftmultiplier), 0); 
    _rightmultiplier = Math.Max(soup.GetNamedTagAsInt("JunctionRightMultiplier", _rightmultiplier), 0);
    int number = soup.GetNamedTagAsInt("JunctionNumber", _number);
    if(type & BRANCH_SYMMETRY) type = type & BRANCH_SYMMETRY;
    else if(type & BRANCH_LEFT) type = type & BRANCH_LEFT;
    _offset = soup.GetNamedTagAsInt("JunctionOffset", _offset);
    _numbervisible = soup.GetNamedTagAsBool("JunctionNumberVisible", _numbervisible);

    //Заглушка для старой версии
    bool manual, autoreset;
    if(soup.GetIndexForNamedTag("JunctionAutoReturn") == -1 and soup.GetIndexForNamedTag("JunctionIsManual") >= 0) {
      autoreset = !soup.GetNamedTagAsBool("JunctionIsManual");
      manual = false;
    } else {
      manual = soup.GetNamedTagAsBool("JunctionIsManual", _manual);
      autoreset = soup.GetNamedTagAsBool("JunctionAutoReturn", _autoreturn);
    }
    if (_configflags & CFG_AUTOJUNCTION) manual = false;
    else if (_configflags & CFG_MANUALJUNCTION) manual = true; 
    
    if(type != _type or manual != _manual or _autoreturn != autoreset ){
      _autoreturn = autoreset;
      _manual = manual;
      _type = type;
      me.OnJunctionTypeChanged();
    }
    if(number != _number){
      _number = number;
      me.OnJunctionNumberChanged(); 
    }

    KUID sundlibrarykuid = soup.GetNamedTagAsKUID("JunctionSoundLibrary");
    string soundfileid = soup.GetNamedTag("JunctionSoundFileId");
    if(sundlibrarykuid and soundfileid != "" and _soundpoint != "") {
      Asset soundlibrary = TrainzScript.FindAsset(sundlibrarykuid);
      if(soundlibrary) {
        Soup soundlibraryconf = soundlibrary.GetConfigSoup().GetNamedSoup("extensions").GetNamedSoup("advancedjunction-sounds");
        Soup sondsconf = soundlibraryconf.GetNamedSoup("sounds").GetNamedSoup(soundfileid);
        if(sondsconf.GetNamedTag("file") != "" and (World.GetCurrentModule() == World.DRIVER_MODULE or SoundLibraryValid(soundlibraryconf, soundlibrary.GetStringTable()))){
          _soundlibrary = soundlibrary;
          _soundfileid = soundfileid;
          return;
        }
      }
    }
    _soundlibrary = null;
    _soundfileid = "";
    ConfirmOffset();
    UpdateNumberLetter();
    ToggleAnimation();
  }

  public void AppendDependencies(KUIDList io_dependencies)
  {
    inherited(io_dependencies);
    if(_soundlibrary and _soundfileid != "") io_dependencies.AddKUID(_soundlibrary.GetKUID()); 
  }

  public void Init(Asset myAsset)
  {
    inherited(myAsset);
    _library = cast<CT_TracksideProvider>TrainzScript.GetLibrary(myAsset.LookupKUIDTable("TracksideScriptLibrary"));
    _toggle = GetFXAttachment("toggle");
    if (!_toggle) _toggle = me;
    Soup configsoup = myAsset.GetConfigSoup().GetNamedSoup("extensions").GetNamedSoup("advancedjunction-settings");
    _configflags = ECLLogic.SetBitMask(_configflags, CFG_CANOFFSET, configsoup.GetNamedTagAsBool("offset-enabled", _configflags & CFG_CANOFFSET));
    _configflags = ECLLogic.SetBitMask(_configflags, CFG_CANNUMBERVISIBLE, configsoup.GetNamedTagAsBool("can-number-visible", _configflags & CFG_CANNUMBERVISIBLE));
    _configflags = ECLLogic.SetBitMask(_configflags, CFG_INVERTANIMATION, configsoup.GetNamedTagAsBool("toggle-animation-invert", _configflags & CFG_INVERTANIMATION));
    if (_configflags & CFG_CANOFFSET) {
      string[] offsetargs = Str.Tokens(configsoup.GetNamedTag("offset-maximum"), ",");
      if (offsetargs.size() == 2) {
        _backwardoffset = Math.Max(Str.ToInt(offsetargs[0]), 0);
        _forwardoffset = Math.Max(Str.ToInt(offsetargs[1]), 0);
      } 
    }
    if (_configflags & CFG_CANNUMBERVISIBLE) {
      _configflags = ECLLogic.SetBitMask(_configflags, CFG_VERTICALNUMBER, configsoup.GetNamedTagAsBool("number-orientation-vertical", _configflags & CFG_VERTICALNUMBER));
      _numberliteralsize = configsoup.GetNamedTagAsFloat("number-letter-size", _numberliteralsize); 
      _numberlitteraseparator = configsoup.GetNamedTagAsFloat("number-letter-separator", _numberlitteraseparator);
      _numberlength = configsoup.GetNamedTagAsInt("number-letter-count", _numberlength); 
      string align = configsoup.GetNamedTag("number-align");
      Str.ToLower(align);
      _configflags = ECLLogic.SetBitMask(_configflags, CFG_NUMBERALIGN | CFG_ALIGNFAR, false);
      if (align == "near") _configflags = ECLLogic.SetBitMask(_configflags, CFG_NUMBERALIGN, true);
      else if (align == "far") _configflags = ECLLogic.SetBitMask(_configflags, CFG_NUMBERALIGN | CFG_ALIGNFAR, true);   
    }
    string jynctiontype = configsoup.GetNamedTag("junction-type");
    _configflags = ECLLogic.SetBitMask(_configflags, CFG_AUTOJUNCTION, jynctiontype == "auto");
    _configflags = ECLLogic.SetBitMask(_configflags, CFG_MANUALJUNCTION, jynctiontype == "manual");
    _toggleanimationmesg = configsoup.GetNamedTag("toggle-animation-mesh");
    _soundpoint = configsoup.GetNamedTag("soundpoint");
    _letter = myAsset.FindAsset("letter");
    _numbers = myAsset.FindAsset("numbers");
    _manual = _configflags & CFG_MANUALJUNCTION;
  //AddHandler(me, "Junction", "Toggled", "ToggledJunctionHandler"); //Несмотря на информацию из junction.gs, данное сообшение не отсылается
  }

};