include "interfaceprovider.gs"
include "mapobject.gs"

class CT_StationNameSigns isclass MapObject, InterfaceProviderBase
{
  Asset library;
  Asset[] tmpLibs;

  string texture;
  string[] tmpTexs;

  bool noreplaceopora = false;

  final void UpdateTexture(string effectcfgname, Asset lib, int index)
  {
    string effect = me.GetAsset().GetConfigSoup().GetNamedSoup("extensions").GetNamedTag(effectcfgname);
    if(effect != "") me.SetFXTextureReplacement(effect, lib, index);
  }

  final void UpdateTextures(void)
  {
    bool replaceopora = library and !noreplaceopora and library.GetConfigSoup().GetNamedSoup("extensions").GetNamedSoup("opora-textures").GetNamedTagAsInt("index", -1) >= 0;
    bool replaceoporanorm = replaceopora and library.GetConfigSoup().GetNamedSoup("extensions").GetNamedSoup("opora-textures").GetNamedTagAsInt("index-normal", -1) >= 0;
    bool replacesigns = library and texture != "" and library.GetConfigSoup().GetNamedSoup("extensions").GetNamedSoup("signs-textures").GetNamedSoup(texture).GetNamedTagAsInt("index", -1) >= 0;
    bool replacesignsnorm = replacesigns and library.GetConfigSoup().GetNamedSoup("extensions").GetNamedSoup("signs-textures").GetNamedSoup(texture).GetNamedTagAsInt("index-normal", -1) >= 0;
    if(replaceopora) me.UpdateTexture("opora-texture-effect-name", library, library.GetConfigSoup().GetNamedSoup("extensions").GetNamedSoup("opora-textures").GetNamedTagAsInt("index"));
    else me.UpdateTexture("opora-texture-effect-name", null, 0);
    if(replaceoporanorm) me.UpdateTexture("opora-texture-normal-effect-name", library, library.GetConfigSoup().GetNamedSoup("extensions").GetNamedSoup("opora-textures").GetNamedTagAsInt("index-normal"));
    else me.UpdateTexture("opora-texture-normal-effect-name", null, 0);
    if(replacesigns) me.UpdateTexture("signs-texture-effect-name", library, library.GetConfigSoup().GetNamedSoup("extensions").GetNamedSoup("signs-textures").GetNamedSoup(texture).GetNamedTagAsInt("index"));
    else me.UpdateTexture("signs-texture-effect-name", null, 0);
    if(replacesignsnorm) me.UpdateTexture("signs-texture-normal-effect-name", library, library.GetConfigSoup().GetNamedSoup("extensions").GetNamedSoup("signs-textures").GetNamedSoup(texture).GetNamedTagAsInt("index-normal"));
    else me.UpdateTexture("signs-texture-normal-effect-name", null, 0);
  }


  final string GetLibraryName(void)
  {
    if(library){
      string tsname = library.GetConfigSoup().GetNamedSoup("extensions").GetNamedTag("title-string"), ret;
      if(tsname != "") ret = library.GetStringTable().GetString(tsname);
      if(ret == "") ret = library.GetLocalisedName();
      return ret;
    }
    return "";
  }

  final string GetTextureName(void)
  {
    if(library and texture != ""){
      string tsname = library.GetConfigSoup().GetNamedSoup("extensions").GetNamedSoup("signs-textures").GetNamedSoup(texture).GetNamedTag("title-string"), ret;
      if(tsname != "") ret = library.GetStringTable().GetString(tsname);
      if(ret == "") ret = "@" + texture;
      return ret;
    }
    return "";
  }


  string GetPropertyType(string propertyID)
  {
    if(propertyID == "library" or propertyID == "texture") return "list,1";
    else if(propertyID == "noreplace") return "link";
    return inherited(propertyID);
  }

  public string[] GetPropertyElementList(string propertyID)
  {
    if(propertyID == "library"){
      string texturegroup = me.GetAsset().GetConfigSoup().GetNamedSoup("extensions").GetNamedTag("texture-group");
      Asset[] list = TrainzScript.GetAssetList("texture-group", "TUSNSTL");
      string[] ret = new string[0];
      tmpLibs = new Asset[0];
      int i, count = list.size();
      for(i = 0; i < count; i++){
        Soup cfg = list[i].GetConfigSoup().GetNamedSoup("extensions");
        if(texturegroup != "" and texturegroup == cfg.GetNamedTag("texture-group")){
          string tsname = cfg.GetNamedTag("title-string"), libname;
          if(tsname != "") libname = list[i].GetStringTable().GetString(tsname);
          if(libname == "") libname = list[i].GetLocalisedName();
          tmpLibs[tmpLibs.size()] = list[i];
          ret[ret.size()] = libname;
        }
      }
      return ret;
    }else if(propertyID == "texture"){
      Soup cfg = library.GetConfigSoup().GetNamedSoup("extensions").GetNamedSoup("signs-textures");
      string[] ret = new string[0];
      tmpTexs = new string[0];
      int i, count = cfg.CountTags();
      for(i = 0; i < count; i++){
        Soup texcfg = cfg.GetNamedSoup(cfg.GetIndexedTagName(i));
        if(texcfg.GetNamedTagAsInt("index", -1) >= 0){
          string tsname = texcfg.GetNamedTag("title-string"), texname;
          if(tsname != "") texname = library.GetStringTable().GetString(tsname);
          if(texname == "") texname = "@" + texture;
          tmpTexs[tmpTexs.size()] = cfg.GetIndexedTagName(i);
          ret[ret.size()] = texname;
        }
      }
      return ret;
    }
    return inherited(propertyID);
  }

  string GetPropertyName(string propertyID)
  {
    if(propertyID == "library") return strTable.GetString("interface-texture-library");
    else if(propertyID == "texture") return strTable.GetString("interface-texture");
    return inherited(propertyID);
  }


  public string GetDescriptionHTML(void)
  {
    StringTable stable = me.GetAsset().GetStringTable();
    string title = stable.GetString("title");
    if(title == "") title = strTable.GetString("title-stationnamesigns");
    int libcelltype = CELL_ERROR, texlibtype = CELL_ERROR;
    if(library and texture != "") texlibtype = CELL_VALUE;
    if(library)libcelltype = CELL_VALUE;
    tmpLibs = null; tmpTexs = null;


    string ret = me.MakeTitle(title) + HTMLWindow.StartTable("width=400");
    ret = ret + HTMLWindow.StartRow() + me.MakeCell(strTable.GetString("setting-general"), "colspan=2", CELL_HEAD) + HTMLWindow.EndRow();
    ret = ret + HTMLWindow.StartRow() + me.MakeLinkCell("library", strTable.GetString("texture-library") + ":", strTable.GetString("tooltip-texture-library"), "", CELL_KEY) +
                me.MakeLinkCell("library", me.GetLibraryName(), strTable.GetString("tooltip-texture-library"), "", libcelltype) + HTMLWindow.EndRow();
    if(library) ret = ret + HTMLWindow.StartRow() + me.MakeLinkCell("texture", strTable.GetString("texture") + ":", strTable.GetString("tooltip-texture"), "", CELL_KEY) +
                me.MakeLinkCell("texture", me.GetTextureName(), strTable.GetString("tooltip-texture"), "", texlibtype) + HTMLWindow.EndRow();
    else ret = ret + HTMLWindow.StartRow() + me.MakeCell(strTable.GetString("texture") + ":", "", CELL_KEY_DISABLED) + me.MakeCell("", "", CELL_KEY) + HTMLWindow.EndRow();
    if(library and library.GetConfigSoup().GetNamedSoup("extensions").GetNamedSoup("opora-textures").GetNamedTagAsInt("index", -1) >= 0){
      ret = ret + HTMLWindow.StartRow() + HTMLWindow.MakeCell("", "colspan=2") + HTMLWindow.EndRow() + HTMLWindow.StartRow() +
                  me.MakeCell(strTable.GetString("setting-extension"), "colspan=2", CELL_HEAD) + HTMLWindow.EndRow();
      ret = ret + HTMLWindow.StartRow() + me.MakeCheckBoxLinkCell("noreplace", strTable.GetString("not-replace-opora-texture"), strTable.GetString("tooltip-not-replace-opora-texture"),
                  noreplaceopora, "colspan=2", CELL_KEY) + HTMLWindow.EndRow();
    }
    return ret + HTMLWindow.EndTable();
  }

  void SetPropertyValue(string propertyID, string value, int index)
  {
    if(propertyID == "library"){
      library = tmpLibs[index];
      texture = "";
      me.UpdateTextures();
    }else if(propertyID == "texture"){
      texture = tmpTexs[index];
      me.UpdateTextures();
    }else inherited(propertyID, value, index);
  }

  void LinkPropertyValue(string propertyID)
  {
    if(propertyID == "noreplace"){
      noreplaceopora = !noreplaceopora;
      me.UpdateTextures();
    }else inherited(propertyID);
  }

  public Soup GetProperties(void)
  {
    Soup soup = inherited();
    if(library and texture != ""){
      soup.SetNamedTag("library", library.GetKUID());
      soup.SetNamedTag("texture", texture);
      soup.SetNamedTag("noreplaceopore", noreplaceopora);
    }
    return soup;
  }

  public void SetProperties(Soup soup)
  {
    inherited(soup);
    KUID libKUID = soup.GetNamedTagAsKUID("library");
    if(libKUID) library = World.FindAsset(libKUID);
    else library = null;
    if(library){
      texture = soup.GetNamedTag("texture");
      if(texture != "" and library.GetConfigSoup().GetNamedSoup("extensions").GetNamedSoup("signs-textures").GetNamedSoup(texture).GetNamedTagAsInt("index", -1) < 0) texture = "";
    }else texture = "";
    noreplaceopora = soup.GetNamedTagAsBool("noreplaceopore");
    me.UpdateTextures();
  }

  public void AppendDependencies(KUIDList io_dependencies)
  {
    inherited(io_dependencies);
    if(library and texture != "") io_dependencies.AddKUID(library.GetKUID());
  }


  public void Init(Asset myAsset)
  {
    inherited(myAsset);
    strTable = myAsset.FindAsset("TracksideScriptLibrary").GetStringTable();
  }




};