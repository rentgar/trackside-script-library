//=============================================================================
// File: ct_distsign.gs
// Desc: Содержит базовую реализацию километровых столбов
// Auth: Алексей 'Эрендир' Зверев 2020 © Trainz Dev Team
//       Licence: GNU GPL
//=============================================================================

include "ct_trackside_sign.gs"

//=============================================================================
// Name: CT_TracksideSign
// Desc: Базовый класс для километровых столбов 
//=============================================================================
class CT_DistantionSign isclass CT_TracksideSign
{

  Asset letter;                 //Актив с мешью для цифр
  Asset numbers;                //Актив с текстурами цифр

  int mileage;                  //Значение километража
  int currfronttanle;           //Текущая отображаемая табличка спереди
  int currreartable;            //Текущая отображаемая табличка сзади
  
  float prevsigndistance;       //Растояние до предыдущего знака  

  bool secondary;               //Указатель, что знак для второстепенных путей


  final string NumberToString(int number)
  {
    if(number == 4)return "four";
    if(number == 3)return "three";
    if(number == 2)return "two";
    return "one";
  }

  final void HideTables(void)
  {
    Soup signconfig = GetAsset().GetConfigSoup().GetNamedSoup("extensions");
    if(signconfig.GetIndexForNamedTag("mileagesign-settings") >= 0) signconfig = signconfig.GetNamedSoup("mileagesign-settings");
    int frontsize = ((string)mileage).size();
    int rearsize = ((string)(mileage + 1)).size();
    string showmesh;
    if(secondary and frontsize != rearsize) showmesh = signconfig.GetNamedTag(NumberToString(frontsize) + "-" + NumberToString(rearsize) +  "-number-table-mesh");
    else showmesh = signconfig.GetNamedTag(NumberToString(rearsize) + "-number-table-mesh");
    int i;
    for(i = 1; i <= 4; ++i) {
      if(secondary and i < 4) {
        string secmesh = signconfig.GetNamedTag(NumberToString(i) + "-" + NumberToString(i + 1) + "-number-table-mesh");
        if(showmesh != secmesh) SetMeshVisible(secmesh, false, 0.0);   
      }
      string hidemesh = signconfig.GetNamedTag(NumberToString(i) + "-number-table-mesh");
      if(showmesh != hidemesh) SetMeshVisible(hidemesh, false, 0.0); 
    }
  }


  //Получение коэфициента смещения
  float GetPositionCoefficient(int frontcount, int rearcount, string side)
  {
    Soup signconfig = GetAsset().GetConfigSoup().GetNamedSoup("extensions");
    if(signconfig.GetIndexForNamedTag("mileagesign-settings") >= 0) signconfig = signconfig.GetNamedSoup("mileagesign-settings");
    string paramname;
    if(frontcount == rearcount) paramname = NumberToString(frontcount);
    else paramname = NumberToString(frontcount) + "-" + NumberToString(rearcount);
    if(side != "") paramname = paramname + "-" + side;
    paramname = paramname + "-position-coefficient";
    return signconfig.GetNamedTagAsFloat(paramname);
  }

  //Получение координат цифр на табличке
  float[] GetLetterPosition(int letterCount)
  {
    Soup signconfig = GetAsset().GetConfigSoup().GetNamedSoup("extensions");
    if(signconfig.GetIndexForNamedTag("mileagesign-settings") >= 0) signconfig = signconfig.GetNamedSoup("mileagesign-settings");
    string configposition = signconfig.GetNamedTag(NumberToString(letterCount) + "-number-position");
    string[] numbers = Str.Tokens(configposition, ",");
    float [] returns = new float[letterCount];
    int i;
    for(i = 0; i < letterCount; ++i) {
      if(i < numbers.size()) returns[i] = Str.ToFloat(numbers[i]);
      else returns[i] = 0.0; 
    }
    return returns;
  }

  //Обновление показаний километрожа
  void UpdateLetter(string effetBaseName, string number, float k)
  {
    float[] positions = GetLetterPosition(number.size());
    int i;
    for(i = 0; i < 4; ++i) {
      if(i < number.size()){
        MeshObject curletter = GetFXAttachment(effetBaseName + (i + 1));
        if(!curletter) curletter = SetFXAttachment(effetBaseName + (i + 1), letter);
        curletter.SetMeshTranslation("default", positions[i] - k, 0.0, 0.0);
        curletter.SetFXTextureReplacement("letter", numbers, Str.ToInt(number[i]) - 48);        
      } else SetFXAttachment(effetBaseName + (i + 1), null);
    }
  }

  void UpdateObject(void)
  {
    string frontnumber = mileage;
    string rearnumber = mileage + 1;
    int maxsimbols = Math.Max(frontnumber.size(), rearnumber.size());
    Soup signconfig = GetAsset().GetConfigSoup().GetNamedSoup("extensions");
    if(signconfig.GetIndexForNamedTag("mileagesign-settings") >= 0) signconfig = signconfig.GetNamedSoup("mileagesign-settings");
    string hidemesh, showmesh;
    if(secondary and frontnumber.size() != maxsimbols) showmesh = signconfig.GetNamedTag(NumberToString(frontnumber.size()) + "-" + NumberToString(maxsimbols) + "-number-table-mesh");
    else showmesh = signconfig.GetNamedTag(NumberToString(maxsimbols) + "-number-table-mesh");
    if(secondary and currreartable > 0 and currfronttanle != currreartable) hidemesh = signconfig.GetNamedTag(NumberToString(currfronttanle) + "-" + NumberToString(currreartable) + "-number-table-mesh");
    else if(currreartable > 0) hidemesh = signconfig.GetNamedTag(NumberToString(currreartable) + "-number-table-mesh");
    currreartable = maxsimbols;    
    currfronttanle = frontnumber.size();
    
    if(showmesh != hidemesh) {
      if(hidemesh != "") SetMeshVisible(hidemesh, false, 0.0);
      SetMeshVisible(showmesh, true, 0.0);
    }
    
    if(secondary and reverse) SetMeshOrientation(showmesh, 0.0, Math.PI, 0.0);
    else if (secondary) SetMeshOrientation(showmesh, 0.0, 0.0, 0.0);
    
    if(reverse) {
      string temp = frontnumber;
      frontnumber = rearnumber;
      rearnumber = temp; 
    }      
    
    if(secondary) UpdateLetter(signconfig.GetNamedTag("front-number-effect-name"), frontnumber, 0.0);
    else UpdateLetter(signconfig.GetNamedTag("front-number-effect-name"), frontnumber, -GetPositionCoefficient(frontnumber.size(), rearnumber.size(), "front"));
    if(secondary) UpdateLetter(signconfig.GetNamedTag("rear-number-effect-name"), rearnumber, signconfig.GetNamedTagAsFloat(NumberToString(rearnumber.size()) + "-position-coefficient"));
    else UpdateLetter(signconfig.GetNamedTag("rear-number-effect-name"), rearnumber, -GetPositionCoefficient(rearnumber.size(), rearnumber.size(), "rear")); 
  }


  //Назначение киллометрожа от последнего установленного
  final void SearchPrevSign(void)
  {
    GSTrackSearch searcher = BeginTrackSearch(false);
    MapObject mapobject = searcher.SearchNext();
    float distance = searcher.GetDistance();
    CT_DistantionSign prevsign = null;
    while(mapobject and distance < 1500 and !prevsign) {
      if(!searcher.GetFacingRelativeToSearchDirection()) prevsign = cast<CT_DistantionSign>mapobject;
      if(!prevsign) {
        mapobject = searcher.SearchNext();
        distance = searcher.GetDistance();
      }
    } 
    if(prevsign) {
      mileage = Math.Fabs(prevsign.mileage) + 1;
      prevsigndistance = distance;
    } else prevsigndistance = -1;
    UpdateObject();
  }

  //Обновление километрожа для всех следующих знаков
  final thread void UpdateFollow(void)
  {
    GSTrackSearch searcher = BeginTrackSearch(true);
    MapObject mapobject = searcher.SearchNext();
    float distance = searcher.GetDistance();
    int count = 0;
    int newvalue = mileage + 1;
    while(mapobject and distance < 1500) {
      if(searcher.GetFacingRelativeToSearchDirection()) {
        CT_DistantionSign nextsign = cast<CT_DistantionSign>mapobject;
        if(nextsign) {
          nextsign.mileage = newvalue++;
          nextsign.UpdateObject();
          searcher = nextsign.BeginTrackSearch(true);
        }
      }
      count++;
      if(count == 1000) {
         Sleep(0.01);
         count = 0;       
      }
      mapobject = searcher.SearchNext();
      distance = searcher.GetDistance();
    }
  }

	public string GetPropertyType(string id)
	{
    if(id == "fromlast" or id == "updatefollowing") return "link";
		else if(id == "setvalue") return "int,0,9998,1";
		return inherited(id);
	}

  string GetPropertyName(string id)
  {
    StringTable strtablethis = GetAsset().GetStringTable(), strtable;
    Asset libasset = GetAsset().FindAsset("TracksideScriptLibrary");
    if(libasset) strtable = libasset.GetStringTable();
    else strtable = strtablethis;
    if(id == "setvalue") return strtable.GetString("interface-setmileage");
    return inherited(id);
  }

  public string GetPropertyValue(string id)
  {
    if(id == "setvalue")return (string)mileage;
    return inherited(id);
  }

  final string GetPrevDescriptionHTML(StringTable strTable, StringTable strTableLocal)
  {
    HTMLBuffer buffer = HTMLBufferStatic.Construct();
    buffer.Print(HTMLWindow.StartTable("border=0"));
      buffer.Print(HTMLWindow.StartRow());
        buffer.Print(HTMLWindow.MakeCell(HTMLWindow.MakeBold(strTable.GetString("settings-mileage")), "colspan=4"));
      buffer.Print(HTMLWindow.EndRow());
      buffer.Print(HTMLWindow.StartRow());
        buffer.Print(HTMLWindow.MakeSpacerCell(10));
        buffer.Print(HTMLWindow.MakeCell(strTable.GetString("setmileage") + ":"));
        buffer.Print(HTMLWindow.MakeCell(HTMLWindow.MakeFontColor(HTMLWindow.MakeLink("live://property/fromlast", strTable.GetString("setmileagefromlast"), strTable.GetString("tooltip-setmileagefromlast")), "007acc")));
        buffer.Print(HTMLWindow.MakeCell(HTMLWindow.MakeFontColor(HTMLWindow.MakeLink("live://property/updatefollowing", strTable.GetString("updatefollowing"), strTable.GetString("tooltip-updatefollowing")), "007acc")));
      buffer.Print(HTMLWindow.EndRow());
      if(prevsigndistance != 0) {
        buffer.Print(HTMLWindow.StartRow());
          buffer.Print(HTMLWindow.MakeSpacerCell(10));
          buffer.Print(HTMLWindow.MakeCell(strTable.GetString("prevsigndist") + ":"));
          if(prevsigndistance > 0) buffer.Print(HTMLWindow.MakeCell(HTMLWindow.MakeFontColor(strTable.GetString1("message-prevsigndist", String.FloatToPercentRoundString(prevsigndistance)), "66ff33"), "colspan=2")); 
          else if(prevsigndistance < 0) buffer.Print(HTMLWindow.MakeCell(HTMLWindow.MakeFontColor(strTable.GetString("message-prevsignnotfound"), "e91717"), "colspan=2")); 
        buffer.Print(HTMLWindow.EndRow());
      }
      buffer.Print(HTMLWindow.StartRow());
        buffer.Print(HTMLWindow.MakeSpacerCell(10));
        buffer.Print(HTMLWindow.MakeCell(strTable.GetString("mileage") + ":"));
        buffer.Print(HTMLWindow.MakeCell(HTMLWindow.MakeLink("live://property/setvalue", mileage, strTable.GetString("tooltip-setmileage"))));
      buffer.Print(HTMLWindow.EndRow());
    buffer.Print(HTMLWindow.EndTable());    
    return buffer.AsString();
  }
  
  public void LinkPropertyValue(string id){
    if(id == "fromlast") SearchPrevSign();
    else if(id == "updatefollowing") me.UpdateFollow();
    else inherited(id);
  }

  void SetPropertyValue(string id, int value)
  {
    if(id == "setvalue"){
      mileage = value;
      UpdateObject();
    }else inherited(id, value);
  }

  public Soup GetProperties(void)
  {
    Soup soup = inherited();
    soup.SetNamedTag("value", mileage);
    return soup;
  }

	public void SetProperties(Soup soup)
  {
    inherited(soup);
    mileage = soup.GetNamedTagAsInt("value", 0);
    prevsigndistance = 0;
    me.HideTables();
    me.UpdateObject();
  }

  public void Init(Asset myAsset)
  {
		inherited(myAsset);
    Soup signconfig = GetAsset().GetConfigSoup().GetNamedSoup("extensions");
    if(signconfig.GetIndexForNamedTag("mileagesign-settings") >= 0) signconfig = signconfig.GetNamedSoup("mileagesign-settings");
    MeshObject mrk = me.GetFXAttachment("marker");
    if(mrk) mrk.SetMeshVisible("piket_mrk", true, 0.0);
    letter = myAsset.FindAsset("letter");
    numbers = myAsset.FindAsset("numbers");
    secondary = signconfig.GetNamedTagAsBool("type-secondary");
  }

};







