include "ct_advancedjunction.gs"
include "trainzassetsearch.gs"
include "library.gs"

class CT_SoundFileEntry isclass GSObject
{
  public string Title;
  public string File;
  public string ConfigId;
};

class CT_SoundLibraryEntry isclass GSObject
{
  public string Title;
  public Asset Asset;
  public CT_SoundFileEntry[] Files;
};


class CT_TracksideProvider isclass Library
{

  CT_SoundLibraryEntry[] _advjunctionsoundlist;

  public CT_SoundLibraryEntry[] GetAdvancedJunctionSoundList()
  {
    return _advjunctionsoundlist;
  }
  
  public CT_SoundLibraryEntry GetAdvancedJunctionSound(int index) {
    if(index >= 0 and index < _advjunctionsoundlist.size()) return _advjunctionsoundlist[index];
    return null;
  }
  


  
  final thread void LoadAdvancedJunctionSoundList(void)
  {
    KUID groupkuid = GetAsset().LookupKUIDTable("adavancedjunction-soundgroup");
    AsyncTrainzAssetSearchObject result = TrainzAssetSearch.NewAsyncSearchObject();
    TrainzAssetSearch.AsyncSearchAssets(TrainzAssetSearch.FILTER_IN_ASSET_GROUP, groupkuid.GetHTMLString(), result);
    _advjunctionsoundlist = new CT_SoundLibraryEntry[0];
    if(result.SynchronouslyWaitForResults()) {
      Asset[] assets = result.GetResults();
      int i, j;
      Interface.Log("Активов: " + assets.size());
      for(i = 0; i < assets.size(); ++i) {
        Soup soundconf = assets[i].GetConfigSoup().GetNamedSoup("extensions").GetNamedSoup("advancedjunction-sounds");
        string titlestring = soundconf.GetNamedTag("title-string");
        Interface.Log("titlestr: " + titlestring);
        if(titlestring == "") continue;
        StringTable strtable = assets[i].GetStringTable();
        string title = strtable.GetString(titlestring);
        Interface.Log("titlestr: " + title);
        if(title == "") continue;
        soundconf = soundconf.GetNamedSoup("sounds");
        int count = soundconf.CountTags();
        Interface.Log("Количество звуков " + count);
        if(count == 0) continue;
        CT_SoundFileEntry[] fileentries = new CT_SoundFileEntry[count];
        bool valid = true;
        for(j = 0; j < count; ++j) {
          Soup fileconf = soundconf.GetNamedSoup(soundconf.GetIndexedTagName(j));
          string filetitlestring = fileconf.GetNamedTag("title-string");
          string filename = fileconf.GetNamedTag("file");
          Interface.Log("Метка " + filetitlestring);
          Interface.Log("Файл " + filename);
          if(filetitlestring == "" or filename == "") {
            valid = false;
            break;
          }
          string filetitle = strtable.GetString(filetitlestring);
          Interface.Log("title файла " + filetitle);
          if(filetitle == "") {
            valid = false;
            break;
          }
          fileentries[j] = new CT_SoundFileEntry();
          fileentries[j].Title = filetitle;
          fileentries[j].File = filename;
          fileentries[j].ConfigId = soundconf.GetIndexedTagName(j);           
        }
        if(valid){
          int index = _advjunctionsoundlist.size();
          _advjunctionsoundlist[index] = new CT_SoundLibraryEntry();
          _advjunctionsoundlist[index].Title = title;
          _advjunctionsoundlist[index].Asset = assets[i];
          _advjunctionsoundlist[index].Files = fileentries; 
        }
      } 
    }
    Interface.Log("Обработано: " + _advjunctionsoundlist.size());
  }



  final void JunctionToggledHandler(Message msg)
  {
    Interface.Log("Обработчик");
    CT_AdvancedJunction jnc = msg.src;
    if(jnc) jnc.ToggledJunctionHandler(msg);
  }
    
    
  public void Init(Asset myAsset)
  {
    inherited(myAsset);
    LoadAdvancedJunctionSoundList();
    me.AddHandler(me, "Junction", "Toggled", "JunctionToggledHandler");
  }

}; 