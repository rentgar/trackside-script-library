include "interfaceprovider.gs"
include "ct_positionitem.gs"
include "trackside.gs"
include "string.gs"

class CT_RailLubricator isclass Trackside, InterfaceProviderBase
{
  CT_PositionItem[] positions = new CT_PositionItem[0];
  Asset[] tmpassets;
  Asset defaultbox;                   //�����, �������������� �������� ���� �� ���������
  Asset custombox;                    //�����, ������� �������� ��������� ������������ �������� ����
  bool canrotate            = false;  //���������, ��� ����� ��������� �� 180 �������� �����������������
  bool cancustombox         = false;  //���������, ��� ����� ���������� ��������� �������� �����
  bool canboxmove           = true;   //���������, ��� ����� ������� �������� ���� �� ���� X � Y
  bool canboxheightoffset   = true;   //���������, ��� ����� �������� ��������� ��������� ����� �� Z
  bool canboxrotate         = true;   //���������, ��� ����� ������� �������� ����
  bool defaultboxview       = true;   //����������, ��� �������� ���� �� ��������� ���������� ���������
  bool rotated;                       //���������, ��� ����������������� ���������
  int boxtype;                        //��� ���������� ��������� �����
  float boxpositionx;                 //��������� ��������� ����� �� X
  float boxpositiony;                 //��������� ��������� ����� �� Y
  float boxpositionz;                 //��������� ��������� ����� �� Z
  float boxoffsetz;                   //�������� ��������� ����� �� Z
  float boxrotate;                    //���� �������� ��������� ����� �� Z
  int boxpositionindex;               //��������� ����������� ������� ��������� �����

  final void UpdateRotate(void)
  {
    if(canrotate){
      if(rotated) me.SetMeshOrientation("default", 0.0, 0.0, Math.PI);
      else me.SetMeshOrientation("default", 0.0, 0.0, 0.0);
    }
  }

  final void UpdateRelayBox(void)
  {
    me.SetMeshTranslation("box-move", boxpositionx, boxpositiony, boxpositionz + boxoffsetz);
    me.SetMeshOrientation("box-move", 0.0, 0.0, boxrotate *  Math.PI / 180);
    if(boxtype == 2 and custombox) me.SetFXAttachment("box-attach", custombox);
    else if(boxtype == 1 and defaultbox) me.SetFXAttachment("box-attach", defaultbox);
    else me.SetFXAttachment("box-attach", null);
  }

  final string GetStringBool(bool stat)
  {
    if(stat) return strTable.GetString("yes");
    return strTable.GetString("no");
  }

  final string GetRelayBoxType(void)
  {
    if(boxtype == 2) return strTable.GetString("relaybox-custom");
    else if(boxtype == 1) return strTable.GetString("relaybox-default");
    return strTable.GetString("none");
  }

  final string GetPositionName(void)
  {
    if(boxpositionindex >= 0 and boxpositionindex < positions.size())return positions[boxpositionindex].Title;
    else if(boxpositionindex < 0) return strTable.GetString("custom-position");
    return "";
  }

  string GetPropertyType(string propertyID)
  {
    if(propertyID == "customrelaybox") return "list,1";
    if(propertyID == "relayboxposition") return "list,0";
    else if(propertyID == "relayboxrotate") return "float,-180.0,180.0";
    else if(propertyID == "relayboxoffsetheight") return "float,-10.0,10.0";
    else if(propertyID == "relayboxpositionx" or propertyID == "relayboxpositiony") return "float,-20.0,20.0";
    else if(propertyID == "reverse" or propertyID == "relaybox"  or propertyID == "relayboxoffsetheightlow"  or propertyID == "relayboxoffsetheightdef") return "link";
    return inherited(propertyID);
  }

  public string GetPropertyValue(string propertyID)
  {
    if(propertyID == "relayboxpositionx") return (string)boxpositionx;
    else if(propertyID == "relayboxpositiony") return (string)boxpositiony;
    else if(propertyID == "relayboxrotate") return (string)boxrotate;
    else if(propertyID == "relayboxoffsetheight") return (string)boxoffsetz;
    return inherited(propertyID);
  }

  public string[] GetPropertyElementList(string propertyID)
  {
    if(propertyID == "customrelaybox"){
      Asset[] sassets = TrainzScript.GetAssetList("scenary", "SCBBOX");
      Asset[] massets = TrainzScript.GetAssetList("mesh", "SCBBOX");
      string[] ret = new string[sassets.size() + massets.size()];
      tmpassets = new Asset[sassets.size() + massets.size()];
      int i;
      for(i = 0; i < sassets.size() + massets.size(); i++){
        if(i >= sassets.size()){
          ret[i] = massets[i - sassets.size()].GetLocalisedName();
          tmpassets[i] = massets[i - sassets.size()];
        }else{
          ret[i] = sassets[i].GetLocalisedName();
          tmpassets[i]  = sassets[i];
        }
      }
      return ret;
    }else if(propertyID == "relayboxposition"){
      int i, count = positions.size(), offset = 0;
      if(canboxmove or canboxheightoffset or canboxrotate) offset = 1;
      string[] ret = new string[count + offset];
      if(offset) ret[0] = strTable.GetString("custom-position");
      for(i = 0; i < count; i++)
        ret[i + offset] = positions[i].Title;
      return ret;
    }
    return inherited(propertyID);
  }

  string GetPropertyName(string propertyID)
  {
    if(propertyID == "relayboxposition") return strTable.GetString("interface-relaybox-position");
    else if(propertyID == "relayboxpositionx") return strTable.GetString("interface-offsetaxis");
    else if(propertyID == "relayboxpositiony") return strTable.GetString("interface-offsetalongaxis");
    else if(propertyID == "relayboxrotate") return strTable.GetString("interface-rotatez");
    else if(propertyID == "relayboxoffsetheight") return strTable.GetString("interface-offsetheight");
    return inherited(propertyID);
  }

  public string GetDescriptionHTML(void)
  {
    tmpassets = null;
    string customrelayboxname;
    if(custombox) customrelayboxname = custombox.GetLocalisedName();
    string ret = me.MakeTitle(strTable.GetString("title-lubricator")) + HTMLWindow.StartTable("width=400");
    ret = ret + HTMLWindow.StartRow() + me.MakeCell(strTable.GetString("settings-outsideview"), "colspan=4", CELL_HEAD) + HTMLWindow.EndRow();
    if(canrotate){
      ret = ret + HTMLWindow.StartRow() + me.MakeCell(strTable.GetString("reverse") + ":", "colspan=3", CELL_KEY) +
                  me.MakeLinkCell("reverse", me.GetStringBool(rotated), strTable.GetString("tooltip-reverse"), "", CELL_VALUE) + HTMLWindow.EndRow();
    }
    if(cancustombox or defaultbox){
      ret = ret + HTMLWindow.StartRow() + me.MakeCell(strTable.GetString("relaybox") + ":", "", CELL_KEY) +
                  me.MakeLinkCell("relaybox", me.GetRelayBoxType(), strTable.GetString("tooltip-relaybox"), "colspan=3", CELL_VALUE) + HTMLWindow.EndRow();
      if(boxtype == 2){
        ret = ret + HTMLWindow.StartRow() + me.MakeLinkCell("customrelaybox", strTable.GetString("relaybox-custom") + ":", strTable.GetString("tooltip-relaybox-custom"), "", CELL_KEY) +
                    me.MakeLinkCell("customrelaybox", customrelayboxname, strTable.GetString("tooltip-relaybox-custom"), "colspan=3", CELL_VALUE) + HTMLWindow.EndRow();
      }
      if(boxtype == 1 or (boxtype == 2 and custombox)){
        ret = ret + HTMLWindow.StartRow() + me.MakeCell(strTable.GetString("relaybox-position") + ":", "", CELL_KEY) +
                    me.MakeLinkCell("relayboxposition", me.GetPositionName(), strTable.GetString("tooltip-relaybox-position"), "colspan=3", CELL_VALUE) + HTMLWindow.EndRow();
        if(boxpositionindex < 0 and canboxmove){
          ret = ret + HTMLWindow.StartRow() + me.MakeCell(strTable.GetString("relaybox-position-x") + ":", "colspan=3 width=340", CELL_KEY) +
                      me.MakeLinkCell("relayboxpositionx", String.FloatToPercentRoundString(boxpositionx), strTable.GetString("tooltip-offsetaxis"), "align=center", CELL_VALUE) + HTMLWindow.EndRow();
          ret = ret + HTMLWindow.StartRow() + me.MakeCell(strTable.GetString("relaybox-position-y") + ":", "colspan=3", CELL_KEY) +
                      me.MakeLinkCell("relayboxpositiony", String.FloatToPercentRoundString(boxpositiony), strTable.GetString("tooltip-offsetalongaxis"), "align=center", CELL_VALUE) + HTMLWindow.EndRow();
        }
        if(boxpositionindex < 0 and canboxrotate){
          ret = ret + HTMLWindow.StartRow() + me.MakeCell(strTable.GetString("relaybox-rotate") + ":", "colspan=3 width=340", CELL_KEY) +
                      me.MakeLinkCell("relayboxrotate", strTable.GetString1("relaybox-rotate-t", String.FloatToPercentRoundString(boxrotate)), strTable.GetString("tooltip-setrotatez"), "align=center", CELL_VALUE) + HTMLWindow.EndRow();
        }
        if((boxpositionindex < 0 and canboxheightoffset) or (boxpositionindex >= 0 and boxpositionindex < positions.size() and positions[boxpositionindex].CanHeightOffset)){
          ret = ret + HTMLWindow.StartRow() + me.MakeCell(strTable.GetString("offsetheight") + ":", "width=220", CELL_KEY) +
                      me.MakeLinkCell("relayboxoffsetheight", String.FloatToPercentRoundString(boxoffsetz), strTable.GetString("tooltip-offsetheight"), "align=center", CELL_VALUE) +
                      me.MakeLinkCell("relayboxoffsetheightlow", "-0.20", strTable.GetString("tooltip-offsetHeightlow"), "align=center", CELL_KEY) +
                      me.MakeLinkCell("relayboxoffsetheightdef", "0.00", strTable.GetString("tooltip-offsetHeightdef"), "align=center", CELL_KEY) + HTMLWindow.EndRow();
        }
      }
    }
    return ret + HTMLWindow.EndTable();
  }

  void SetPropertyValue(string propertyID, string value, int index)
  {
    if(propertyID == "customrelaybox"){
      custombox = tmpassets[index];
      me.UpdateRelayBox();
    }else if(propertyID == "relayboxposition"){
      int offset = 0;
      if(canboxmove or canboxheightoffset or canboxrotate) offset = 1;
      if(offset == 0 or index > 0){
        boxpositionindex = index - offset;
        boxpositionx  = positions[boxpositionindex].PositionX;
        boxpositiony  = positions[boxpositionindex].PositionY;
        boxpositionz  = positions[boxpositionindex].PositionZ;
        boxrotate     = positions[boxpositionindex].Rotate;
        boxoffsetz    = 0.0;
      }else{
        boxoffsetz = boxoffsetz + boxpositionz;
        boxpositionz = 0.0;
        boxpositionindex = -1;
      }
      me.UpdateRelayBox();
    }else inherited(propertyID, value, index);
  }

  void SetPropertyValue(string propertyID, float value)
  {
    if(propertyID == "relayboxpositionx"){
      if(boxpositionindex < 0){
        boxpositionx = value;
        me.UpdateRelayBox();
      }
    }else if(propertyID == "relayboxpositiony"){
      if(boxpositionindex < 0){
        boxpositiony = value;
        me.UpdateRelayBox();
      }
    }else if(propertyID == "relayboxrotate"){
      if(boxpositionindex < 0){
        boxrotate = value;
        me.UpdateRelayBox();
      }
    }else if(propertyID == "relayboxoffsetheight"){
      boxoffsetz = value;
      me.UpdateRelayBox();
    }
    else inherited(propertyID, value);
  }

  void LinkPropertyValue(string propertyID)
  {
    if(propertyID == "reverse"){
      if(canrotate){
        rotated = !rotated;
        me.UpdateRotate();
      }
    }else if(propertyID == "relaybox"){
      boxtype++;
      if(boxtype == 1 and !defaultbox) boxtype++;
      if(boxtype == 2 and !cancustombox) boxtype++;
      if(boxtype > 2) boxtype = 0;
      me.UpdateRelayBox();
    }else if(propertyID == "relayboxoffsetheightlow"){
      boxoffsetz = -0.2;
      me.UpdateRelayBox();
    }else if(propertyID == "relayboxoffsetheightdef"){
      boxoffsetz = 0.0;
      me.UpdateRelayBox();
    }else inherited(propertyID);
  }

	public Soup GetProperties(void)
	{
		Soup soup = inherited();
    if(boxtype != 2) custombox = null;
    if(boxtype == 2 and !custombox) boxtype = 0;
    if(boxtype == 0){
      boxpositionindex = 0;
      boxpositionx  = 4.0;
      boxpositiony  = 0.0;
      boxpositionz  = 0.0;
      boxrotate     = 0.0;
      boxoffsetz    = 0.0;
    }
    soup.SetNamedTag("Rotated", rotated);
    soup.SetNamedTag("BoxType", boxtype);
    soup.SetNamedTag("BoxPositionX", boxpositionx);
    soup.SetNamedTag("BoxPositionY", boxpositiony);
    soup.SetNamedTag("BoxPositionZ", boxpositionz);
    soup.SetNamedTag("BoxOffsetZ", boxoffsetz);
    soup.SetNamedTag("BoxRotate", boxrotate);
    soup.SetNamedTag("BoxPositionIndex", boxpositionindex);
    if(boxtype == 2)soup.SetNamedTag("CustomBox", custombox.GetKUID());
		return soup;
	}

  public void SetProperties(Soup soup)
	{
		inherited(soup);
    rotated = canrotate and soup.GetNamedTagAsBool("Rotated", false);
    if(defaultboxview and defaultbox) boxtype = soup.GetNamedTagAsInt("BoxType", 1);
    else boxtype = soup.GetNamedTagAsInt("BoxType", 0);
    if(boxtype < 0 or boxtype > 2 or (boxtype == 1 and !defaultbox) or (boxtype == 2 and !cancustombox)){
      if(boxtype == 2 and defaultboxview and defaultbox) boxtype = 1;
      else boxtype = 0;
    }
    boxpositionx = soup.GetNamedTagAsFloat("BoxPositionX", 4.0);
    boxpositiony = soup.GetNamedTagAsFloat("BoxPositionY", 0.0);
    boxpositionz = soup.GetNamedTagAsFloat("BoxPositionZ", 0.0);
    boxoffsetz = soup.GetNamedTagAsFloat("BoxOffsetZ", 0.0);
    boxrotate = soup.GetNamedTagAsFloat("BoxRotate", 0.0);
    boxpositionindex = soup.GetNamedTagAsInt("BoxPositionIndex", 0);
    if(boxpositionindex < 0){
      if(!canboxmove){
        boxpositionx = 4.0;
        boxpositiony = 0.0;
      }
      if(!canboxheightoffset){
        boxpositionz = 0.0;
        boxoffsetz = 0.0;
      }
      if(!canboxrotate)boxrotate = 0.0;
    }
    if(boxpositionindex < 0 or boxpositionindex >= positions.size()) boxpositionindex = -1;
    if(boxtype == 2){
      KUID boxkuid = soup.GetNamedTagAsKUID("CustomBox");
      if(boxkuid) custombox = World.FindAsset(boxkuid);
      else custombox = null;
      if(!custombox){
        if(defaultboxview and defaultbox) boxtype = 1;
        else boxtype = 0;
      }
    }
    me.UpdateRotate();
    me.UpdateRelayBox();
  }

  public void AppendDependencies(KUIDList io_dependencies)
  {
    inherited(io_dependencies);
    if(boxtype and custombox) io_dependencies.AddKUID(custombox.GetKUID());
  }

  public void Init(Asset myAsset)
  {
    inherited(myAsset);
    Soup config = myAsset.GetConfigSoup().GetNamedSoup("extensions");
    canrotate           = config.GetNamedTagAsBool("can-rotate", canrotate);
    cancustombox        = config.GetNamedTagAsBool("can-custombox", cancustombox);
    canboxmove          = config.GetNamedTagAsBool("can-box-move", canboxmove);
    canboxheightoffset  = config.GetNamedTagAsBool("can-box-heightofset", canboxheightoffset);
    canboxrotate        = config.GetNamedTagAsBool("can-box-rotate", canboxrotate);
    defaultboxview      = config.GetNamedTagAsBool("box-default-view", defaultboxview);
    if(config.GetIndexForNamedTag("box-default") >= 0) defaultbox = myAsset.FindAsset(config.GetNamedTag("box-default"));
    Soup positionconfig = config.GetNamedSoup("box-positions");
    int i, count = positionconfig.CountTags();
    StringTable strtable = myAsset.GetStringTable();
    for(i = 0; i < count; ++i){
      Soup posconfig = positionconfig.GetNamedSoup(positionconfig.GetIndexedTagName(i));
      string title = strtable.GetString(posconfig.GetNamedTag("Name"));
      String.Trim(title);
      if(title != "") positions[positions.size()] = new CT_PositionItem().Init(title, posconfig.GetNamedTagAsFloat("position-x", 4.0), posconfig.GetNamedTagAsFloat("position-y", 0.0), posconfig.GetNamedTagAsFloat("poxition-z", 0.0), posconfig.GetNamedTagAsFloat("rotate", 0.0), posconfig.GetNamedTagAsBool("can-heightofset", true));
    }
    strTable = myAsset.FindAsset("TracksideScriptLibrary").GetStringTable();
  }

};