//=============================================================================
// File: ct_soundhorn.gs
// Desc: Содержит базовую реализацию для знаков свистка "C"
// Auth: Алексей 'Эрендир' Зверев 2020 © Trainz Dev Team
//       Licence: GNU GPL
//=============================================================================

include "ct_trackside_sign.gs"
                                                                  
//=============================================================================
// Name: CT_SoundHornZnak
// Desc: Класс с реалиацией знаков подачи звукового сигнала "С" 
//=============================================================================
class CT_SoundHornZnak isclass CT_TracksideSign {

  //Типы звуковых сигналов
  define int ST_NONE        = 0;    //Сигнал отсутствует
  define int ST_HORN        = 1;    //Громкий сигнал (Тифон)
  define int ST_WHISTLE     = 2;    //Тихий сигнал (Свисток)

  int[] typesignal;                 //Тут настроеные типы сигналов
  bool[] aionly;                    //Тут указатели необходимости реагирования только на ботов
  bool movedisable;                 //Запрещает перемещение
  
  final string[] GetTrainPriorityNames(void)
  {
    string[] returns = new string[3];
    returns[0] = "pass";
    returns[1] = "cargo";
    returns[2] = "shunt";
    return returns;
  }
  
	final string GetDescriptionSignalType(int type, StringTable strTable)
	{
		if(type == ST_HORN) return strTable.GetString("soundsignal-type-horn");
    else if(type == ST_WHISTLE) return strTable.GetString("soundsignal-type-whistle");
		return strTable.GetString("soundsignal-type-none");
	}

	final string GetDescriptionAI(bool onlyAi, StringTable strTable)
	{
		if(onlyAi) return strTable.GetString("soundsignal-aionly");
		return strTable.GetString("soundsignal-all");
	}
  
	public string GetPropertyType(string id)
	{
    if(id[0, 8] == "settype-" or id[0, 11] == "sethandler-") return "link";
		return inherited(id);
	}

  string GetPrevDescriptionHTML(StringTable strTable, StringTable strTableLocal)
  {
    HTMLBuffer buffer = HTMLBufferStatic.Construct();
    buffer.Print(HTMLWindow.StartTable("border=0"));
      buffer.Print(HTMLWindow.StartRow());
        buffer.Print(HTMLWindow.MakeCell(HTMLWindow.MakeBold(strTable.GetString("settings-soundsign")), "colspan=4"));
      buffer.Print(HTMLWindow.EndRow());
      string[] prioritynames = GetTrainPriorityNames(); 
      int i;
      for(i = 0; i < 3; ++i) {
        buffer.Print(HTMLWindow.StartRow());
          buffer.Print(HTMLWindow.MakeSpacerCell(10));
          buffer.Print(HTMLWindow.MakeCell(strTable.GetString("trainpriority-" + prioritynames[i]) + ":"));
          buffer.Print(HTMLWindow.MakeCell(HTMLWindow.MakeLink("live://property/settype-" + i, GetDescriptionSignalType(typesignal[i], strTable), strTable.GetString("tooltip-soundsignal-type"))));
          if(typesignal[i] != ST_NONE)
            buffer.Print(HTMLWindow.MakeCell(HTMLWindow.MakeLink("live://property/sethandler-" + i, GetDescriptionAI(aionly[i], strTable), strTable.GetString("tooltip-soundsignal-handler"))));
        buffer.Print(HTMLWindow.EndRow());
      }
    buffer.Print(HTMLWindow.EndTable());    
    return buffer.AsString();
  }

	string GetDescriptionHTML(void)
	{
    if(movedisable) {
      StringTable strtablethis = GetAsset().GetStringTable(), strtable;
      Asset libasset = GetAsset().FindAsset("TracksideScriptLibrary");
      if(libasset) strtable = libasset.GetStringTable();
      else strtable = strtablethis;
      HTMLBuffer buffer = HTMLBufferStatic.Construct();
      string title = strtablethis.GetString("title");
      if(title == "") title = strtable.GetString("title-sign"); 
      buffer.Print("<font size=10 color=#31859c><b>");
      buffer.Print(title);
      buffer.Print("</b></font><br><br>");
      buffer.Print(GetPrevDescriptionHTML(strtable, strtablethis));
      return buffer.AsString();
    }
    return inherited();
  }

	void LinkPropertyValue(string id){
    if(id[0, 8] == "settype-") {
      int index = Str.ToInt(id[8,]);
      if(++typesignal[index] > ST_WHISTLE) typesignal[index] = ST_NONE;
    } else if(id[0, 11] == "sethandler-") {
      int index = Str.ToInt(id[11,]);
      aionly[index] = !aionly[index];      
    }else inherited(id);
	}

  //Выполняет проверку наличия требуемого поезда перед знаком
  final bool CheckTrain(Train train)
  {
    GSTrackSearch searcher = BeginTrackSearch(false);
    MapObject mapobject = searcher.SearchNext();
    float distance = searcher.GetDistance();
    while(mapobject and distance <= 30 and !mapobject.isclass(Vehicle)){
      mapobject = searcher.SearchNext();
      distance = searcher.GetDistance();
    }
    return (mapobject and mapobject.isclass(Vehicle) and (cast<Vehicle>mapobject).GetMyTrain() == train);
  }

	final thread void PlaySoundSignal(Train train)
  {
		int priority = train.GetTrainPriorityNumber();
		bool manual = train.GetAutopilotMode() == Train.CONTROL_MANUAL;
    if(!manual or !aionly[priority]) {
      if(typesignal[priority] == ST_WHISTLE) {
        train.SendMessage(train, "loco", "bell");
        train.Sleep(1.1);
        train.SendMessage(train, "loco", "bell");
      } else if(typesignal[priority] == ST_HORN) train.SoundHorn();
    }
	}

	final void EnterHandler(Message msg)
	{
    if(msg.src and msg.src.isclass(Train)){
      Train train = cast<Train>(msg.src);
      if(me.CheckTrain(train)) me.PlaySoundSignal(train);
    }
	}
  
	public Soup GetProperties(void)
	{
		Soup soup = inherited();
    string[] prioritynames = GetTrainPriorityNames(); 
    int i;
    for(i = 0; i < 3; ++i) {
		  soup.SetNamedTag(prioritynames[i] + "SType", typesignal[i]);
		  soup.SetNamedTag(prioritynames[i] + "AIOnly", aionly[i]);
    }
		return soup;
	}
  
	public void SetProperties(Soup soup)
	{
		inherited(soup);
    string[] prioritynames = GetTrainPriorityNames(); 
    int i;
    for(i = 0; i < 3; ++i) {
		  typesignal[i] = Math.Max(Math.Min(soup.GetNamedTagAsInt(prioritynames[i] + "SType", typesignal[i]), ST_WHISTLE), ST_NONE);
		  aionly[i] = soup.GetNamedTagAsBool(prioritynames[i] + "AIOnly", aionly[i]);
    }
    if(movedisable){
      offsetaxis = 0.0;
      offestalong = 0.0;
      offsetheight = 0.0;
      rotatex = 0.0;
      rotatey = 0.0;
      rotatez = 0.0;
      reverse = false;
      UpdateObjectOffset();
    }
  }  

	public void Init(Asset myAsset)
  {
    inherited(myAsset);
    
    //Типы сигналов
    typesignal = new int[3];
    typesignal[0] = ST_HORN;
    typesignal[1] = ST_HORN;
    typesignal[2] = ST_WHISTLE;
          
    //Реакция на игрока
    aionly = new bool[3];
    aionly[0] = true;    
    aionly[1] = true;    
    aionly[2] = true;
    
    Soup signconfig = GetAsset().GetConfigSoup().GetNamedSoup("extensions");
    if(signconfig.GetIndexForNamedTag("mileagesign-settings") >= 0) {
      signconfig = signconfig.GetNamedSoup("mileagesign-settings");
      movedisable = signconfig.GetNamedTagAsBool("OffsetDisabled", false); 
    } else movedisable = !signconfig.GetNamedTagAsBool("canmove", true);  

  	AddHandler(me, "Object", "Enter", "EnterHandler");
  }

};